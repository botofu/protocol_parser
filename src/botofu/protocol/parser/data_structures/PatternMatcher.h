#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_PATTERNMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_PATTERNMATCHER_H

#include <memory>
#include <queue>
#include <vector>

#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/MethodBody.h"
#include "botofu/swf/parser/abc/MethodBody/AVM2Instructions.h"
#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"

namespace {

    using ComparedType = std::pair<std::size_t /*index of first match*/,
                                   std::shared_ptr<BaseComposedMatcher> /*matcher*/>;

    struct BaseComposedMatcherComparator {
        /**
         * @brief Comparator used to sort in the priority queue.
         *
         * From https://en.cppreference.com/w/cpp/container/priority_queue :
         *
         * Note that the Compare parameter is defined such that it returns true if its first
         * argument comes before its second argument in a weak ordering. But because the priority
         * queue outputs largest elements first, the elements that "come before" are actually output
         * last. That is, the front of the queue contains the "last" element according to the weak
         * ordering imposed by Compare.
         *
         * We want the earliest match (smallest index in .first) to be on top of the queue, i.e. to
         * come first. In case of equality, the longest matcher should come first.
         *
         * @param lhs first element to compare
         * @param rhs second element to compare
         * @return true if lhs should come after rhs in pops, otherwise false.
         */
        bool operator()(ComparedType const &lhs,
                        ComparedType const &rhs) const;
    };
}

class PatternMatcher {

public:
    PatternMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                   const DofusMethod &method,
                   abc::MethodBody const &method_body);

    /**
     * @brief Add the given matcher to the list of matchers considered
     * @param matcher the matcher to add
     */
    void add_matcher(const std::shared_ptr<BaseComposedMatcher> &matcher);

    /**
     * @brief Add an instance of @p MatcherType constructed with @p args
     * @tparam MatcherType type of the matcher to add
     * @tparam Args types of the arguments forwarded to the matcher constructor
     * @param args arguments forwarded to the matcher constructor
     */
    template <typename MatcherType, typename... Args>
    void add_matcher(Args... args);

    /**
     * @brief Match all the patterns in the instructions given at instance construction and update
     *        @p fields accordingly
     * @param fields structure containing the information on fields
     */
    void match_all(BaseComposedMatcher::FieldsType &fields);

private:

    void refresh_matchers();

    std::vector<Instr> const                           m_code;
    std::priority_queue<ComparedType,
                        std::vector<ComparedType>,
                        BaseComposedMatcherComparator> m_matchers;
    std::size_t                                        m_position;
    DofusMethod const                                  m_method;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_PATTERNMATCHER_H
