#include "VersionFinder.h"

#include <regex>
#include <fstream>

#include <spdlog/spdlog.h>
#include <boost/filesystem.hpp>

#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"
#include "botofu/protocol/parser/matchers/core/VersionCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/BuildDateCoreMatcher.h"
#include "botofu/protocol/parser/version.h"

static std::unordered_map<std::string, std::size_t> MONTHS_NUMBERS = {
        {"Jan", 1},
        {"Feb", 2},
        {"Mar", 3},
        {"Apr", 4},
        {"May", 5},
        {"Jun", 6},
        {"Jul", 7},
        {"Aug", 8},
        {"Sep", 9},
        {"Oct", 10},
        {"Nov", 11},
        {"Dec", 12},
};

static inline Date extract_protocol_date(std::string const &protocol_date) {
    std::regex  protocol_date_regex(R"([[:alpha:]]{3}, ([[:digit:]]+) ([[:alpha:]]{3}) )"
                                    R"(([[:digit:]]+) ([[:digit:]]+):([[:digit:]]+):[[:digit:]]+)",
                                    std::regex_constants::extended);
    std::smatch protocol_date_match;
    if (!std::regex_search(protocol_date, protocol_date_match, protocol_date_regex)) {
        SPDLOG_WARN("Protocol build date could not be retrieved from the string '{}'. "
                    "A date filled with zeros will be used.", protocol_date);
        return Date{ };
    }
    return Date{std::stoul(protocol_date_match[3]),
                MONTHS_NUMBERS[protocol_date_match[2]],
                std::stoul(protocol_date_match[1]),
                std::stoul(protocol_date_match[4]),
                std::stoul(protocol_date_match[5])};
}

static inline Version find_build_version(std::string const &dofus_invoker_file_path_str) {
    fs::path const dofus_invoker_path(dofus_invoker_file_path_str);
    fs::path const version_file_path(dofus_invoker_path.parent_path() / "VERSION");

    if (!fs::is_regular_file(version_file_path)) {
        SPDLOG_WARN("VERSION file not found in '{}'. Dofus client build version will be set to 0.",
                    version_file_path.string());
        return Version{0, 0, 0, 0};
    }
    std::ifstream input(version_file_path.string());
    std::string   version_file_content(std::istreambuf_iterator<char>(input), { });
    std::regex    version_regex(R"(([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+))",
                                std::regex_constants::extended);
    std::smatch   version_match;
    if (!std::regex_search(version_file_content, version_match, version_regex)) {
        SPDLOG_WARN("Could not match content of VERSION file '{}'. "
                    "Dofus client build version will be set to 0.", version_file_content);
        return Version{0, 0, 0, 0};
    }
    return Version{std::stoul(version_match[1]),
                   std::stoul(version_match[2]),
                   std::stoul(version_match[3]),
                   std::stoul(version_match[4])};
}

VersionFinder::VersionFinder(TagDoAbc const &tag_do_abc, std::string const &dofus_invoker_file_path)
        : build_version{ },
          build_type{BuildType::UNKNOWN},
          build_date{ },
          protocol_version{0},
          protocol_minimum_version{0},
          protocol_date{ } {

    abc::ConstantPoolInfo const &constant_pool_info{tag_do_abc.m_constant_pool_info};
    // 1. Find the method_info of the DofusClientMain constructor
    std::size_t                 method_info_idx{0};
    for (; method_info_idx < tag_do_abc.m_method_count; ++method_info_idx) {
        DofusMethod const method_name(tag_do_abc.m_methods_info[method_info_idx].get_name(
                constant_pool_info));
        if (method_name.get_class_name() == "DofusClientMain" &&
            method_name.get_method_name() == "DofusClientMain") {
            break;
        }
    }

    // 2. Find the method body
    std::size_t method_body_idx{0};
    for (; method_body_idx < tag_do_abc.m_method_bodies.size(); ++method_body_idx) {
        if (tag_do_abc.m_method_bodies[method_body_idx].m_method == method_info_idx) {
            break;
        }
    }

    // 3. Disassemble the method body
    abc::MethodBody const &method_body{tag_do_abc.m_method_bodies[method_body_idx]};
    std::vector<Instr>    method_instructions{method_body.disassemble()};

    // 4. Iterate over the instructions and find the information we want
    // 4.1. Find the version
    std::size_t const past_version_idx{this->extract_version(method_instructions,
                                                             constant_pool_info,
                                                             dofus_invoker_file_path)};
    // 4.2. Find the build date
    this->extract_build_date(method_instructions, past_version_idx, constant_pool_info);

    // 5. Recover the protocol version
    // 5.1. Find the com.ankamagames.dofus.network.Metadata instance_info
    std::size_t          metadata_class_idx{0};
    for (; metadata_class_idx < tag_do_abc.m_instances.size(); ++metadata_class_idx) {
        if (tag_do_abc.m_instances[metadata_class_idx].is_in_network_namespace(constant_pool_info)) {
            DofusType instance_info(tag_do_abc.m_instances[metadata_class_idx].get_name(
                    constant_pool_info));
            if (instance_info.get_type() == "Metadata") {
                break;
            }
        }
    }
    // 5.2. Iterate over the class traits and get the protocol version / date
    abc::ClassInfo const &metadata_class_info{tag_do_abc.m_classes[metadata_class_idx]};
    for (auto const      &trait : metadata_class_info.m_traits) {
        std::string const trait_name{trait.get_name(constant_pool_info)};
        if (trait_name == "PROTOCOL_BUILD") {
            this->protocol_version = std::stoul(trait.get_data(constant_pool_info));
        }
        if (trait_name == "PROTOCOL_DATE") {
            std::string const protocol_date_str(trait.get_data(constant_pool_info));
            this->protocol_date = extract_protocol_date(protocol_date_str);
        }
        if (trait_name == "PROTOCOL_REQUIRED_BUILD") {
            this->protocol_minimum_version = std::stoul(trait.get_data(constant_pool_info));
        }
    }
}

std::size_t VersionFinder::extract_version(std::vector<Instr> const &instructions,
                                           abc::ConstantPoolInfo const &constant_pool_info,
                                           std::string const &dofus_invoker_path_str) {
    // First, try to extract the version from the VERSION file
    this->build_version = find_build_version(dofus_invoker_path_str);

    // Then extract the version from the provided instructions
    VersionCoreMatcher version_matcher(constant_pool_info);
    std::size_t const  match_index{version_matcher.find_pattern_and_update(instructions)};

    // Check that the versions agree.
    #define BOTOFU_CHECK_VERSION_COMPOSANT(VERSION_COMPOSANT)                              \
    if(this->build_version.VERSION_COMPOSANT != version_matcher.VERSION_COMPOSANT) {       \
        SPDLOG_WARN("Found major version '{}' in VERSION file and major version '{}' in "  \
                    "DofusInvoker.swf. Preferring major version from VERSION file: '{}'.", \
                    this->build_version.VERSION_COMPOSANT,                                 \
                    version_matcher.VERSION_COMPOSANT,                                     \
                    this->build_version.VERSION_COMPOSANT);                                \
    }
    BOTOFU_CHECK_VERSION_COMPOSANT(major)
    BOTOFU_CHECK_VERSION_COMPOSANT(minor)
    BOTOFU_CHECK_VERSION_COMPOSANT(patch)
    #undef BOTOFU_CHECK_VERSION_COMPOSANT
    this->build_type = version_matcher.build_type;
    return match_index;
}

std::size_t
VersionFinder::extract_build_date(std::vector<Instr> const &instructions,
                                  std::size_t start_idx,
                                  abc::ConstantPoolInfo const &constant_pool_info) {
    BuildDateCoreMatcher build_date_matcher(constant_pool_info);
    std::size_t const    match_index{build_date_matcher.find_pattern_and_update(instructions,
                                                                                start_idx)};
    this->build_date = build_date_matcher.date;
    return match_index;
}

json VersionFinder::to_json() const {
    json j;
    j["parser"]                  = {{"major", PROTOCOL_PARSER_MAJOR_VERSION},
                                    {"minor", PROTOCOL_PARSER_MINOR_VERSION}};
    j["client"]["version"]       = this->build_version;
    j["client"]["build"]["date"] = this->build_date;
    j["client"]["build"]["type"] = build_type_to_string(this->build_type);
    j["protocol"]["date"]        = this->protocol_date;
    j["protocol"]["version"]     = {{"current", this->protocol_version},
                                    {"minimum", this->protocol_minimum_version}};
    return j;
}
