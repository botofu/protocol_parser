#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_ENUMINFORMATION_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_ENUMINFORMATION_H

#include <vector>
#include <string>
#include <memory>

#include <nlohmann/json.hpp>

#include "botofu/ios/types.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"
#include "botofu/protocol/parser/dofus_interface/DofusType.h"

using json = nlohmann::json;

class EnumInformation {

public:
    EnumInformation(TagDoAbc const &tag, uint32 index);

    [[nodiscard]] json to_json() const;

private:
    DofusType                                                           m_enum_type;
    DofusType                                                           m_member_type;
    std::vector<std::pair<std::string /*name*/, std::string /*value*/>> m_members;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_ENUMINFORMATION_H
