#include "Date.h"

void to_json(json &j, Date const &date) {
    j["year"]   = date.year;
    j["month"]  = date.month;
    j["day"]    = date.day;
    j["hour"]   = date.hour;
    j["minute"] = date.minute;
    j["full"] =
            std::to_string(date.year) + "-" + std::to_string(date.month) + "-" +
            std::to_string(date.day) + " " + std::to_string(date.hour) + ":" +
            std::to_string(date.minute);
}
