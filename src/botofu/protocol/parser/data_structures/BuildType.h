#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_BUILDTYPE_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_BUILDTYPE_H

#include <nlohmann/json.hpp>

using json = nlohmann::json;

enum struct BuildType {
    RELEASE,
    DEBUG,
    UNKNOWN
};

std::string build_type_to_string(BuildType const &build_type);

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_BUILDTYPE_H
