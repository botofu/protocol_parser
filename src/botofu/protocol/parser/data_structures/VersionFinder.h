#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_VERSIONFINDER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_VERSIONFINDER_H

#include <string>
#include <array>

#include <nlohmann/json.hpp>

#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"
#include "botofu/protocol/parser/data_structures/Date.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"
#include "botofu/protocol/parser/data_structures/Version.h"
#include "botofu/protocol/parser/data_structures/BuildType.h"

using json = nlohmann::json;

struct VersionFinder {

    VersionFinder(TagDoAbc const &tag_do_abc, std::string const &dofus_invoker_file_path);

    [[nodiscard]] json to_json() const;

    Version     build_version;
    BuildType   build_type;
    Date        build_date;
    std::size_t protocol_version;
    std::size_t protocol_minimum_version;
    Date        protocol_date;

private:

    std::size_t extract_version(std::vector<Instr> const &instructions,
                                abc::ConstantPoolInfo const &constant_pool_info,
                                std::string const &dofus_invoker_file_path);

    std::size_t extract_build_date(std::vector<Instr> const &instructions,
                                   std::size_t start_idx,
                                   abc::ConstantPoolInfo const &constant_pool_info);

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_VERSIONFINDER_H
