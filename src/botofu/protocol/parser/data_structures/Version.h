#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_VERSION_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_VERSION_H

#include <cstddef>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

struct Version {

    std::size_t major;
    std::size_t minor;
    std::size_t patch;
    std::size_t build;

};

void to_json(json &j, Version const &version);

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_VERSION_H
