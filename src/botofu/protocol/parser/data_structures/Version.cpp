#include "Version.h"

void to_json(json &j, Version const &version) {
    j["major"] = version.major;
    j["minor"] = version.minor;
    j["patch"] = version.patch;
    j["build"] = version.build;
}
