#include "ClassField.h"

#include "botofu/protocol/parser/data_structures/json_helpers.h"

ClassField::ClassField()
        : name(),
          type(),
          default_value(),
          self_serialize_method(""),
          write_type_id_method(""),
          write_method(""),
          write_length_method(""),
          write_false_if_null_method(""),
          bounds(),
          boolean_byte_wrapper_position(-1),
          constant_length(0),
          prefixed_by_type_id{false},
          use_boolean_byte_wrapper{false},
          null_checked{false} {

}

json ClassField::to_json(const boost::optional<json> &default_values) const {
    json j;
    insert_value(default_values, j, "name", this->name.get_attribute_name());
    insert_value(default_values, j, "namespace", this->name.get_namespace().to_string());
    insert_value(default_values, j, "type", this->type.get_type());
    insert_value(default_values, j, "type_namespace", this->type.get_namespace().to_string());
    insert_value(default_values, j, "default_value", this->default_value);
    insert_value(default_values,
                 j,
                 "self_serialize_method",
                 this->self_serialize_method.get_method_name());
    insert_value(default_values,
                 j,
                 "write_type_id_method",
                 this->write_type_id_method.get_method_name());
    insert_value(default_values, j, "write_method", this->write_method.get_method_name());
    insert_value(default_values,
                 j,
                 "write_length_method",
                 this->write_length_method.get_method_name());
    insert_value(default_values,
                 j,
                 "write_false_if_null_method",
                 this->write_false_if_null_method.get_method_name());
    insert_value(default_values, j, "bounds", this->bounds);
    insert_value(default_values, j, "use_boolean_byte_wrapper", this->use_boolean_byte_wrapper);
    insert_value(default_values,
                 j,
                 "boolean_byte_wrapper_position",
                 this->boolean_byte_wrapper_position);
    insert_value(default_values, j, "constant_length", this->constant_length);
    insert_value(default_values, j, "prefixed_by_type_id", this->prefixed_by_type_id);
    insert_value(default_values, j, "null_checked", this->null_checked);
    return j;
}
