target_sources(protocol_parser PUBLIC
        BuildType.cpp
        ClassField.cpp
        ClassInformation.cpp
        Date.cpp
        EnumInformation.cpp
        PatternMatcher.cpp
        Version.cpp
        VersionFinder.cpp
        )
target_sources(protocol_parser INTERFACE
        BuildType.h
        ClassField.h
        ClassInformation.h
        Date.h
        EnumInformation.h
        PatternMatcher.h
        Version.h
        VersionFinder.h
        json_helpers.h
        )