#include "ClassInformation.h"

#include <unordered_set>

#include <boost/algorithm/string/predicate.hpp>

#include "botofu/swf/parser/abc/ClassInfo.h"
#include "botofu/swf/parser/abc/InstanceInfo.h"
#include "botofu/swf/parser/abc/MethodInfo.h"
#include "botofu/swf/parser/abc/MethodBody.h"
#include "botofu/protocol/parser/data_structures/PatternMatcher.h"

using boost::algorithm::starts_with;

ClassInformation::ClassInformation(TagDoAbc const &tag, std::size_t index)
        : m_protocol_id{0},
          m_use_hash_function{false} {
    abc::ClassInfo const        &class_info{tag.m_classes[index]};
    abc::InstanceInfo const     &instance_info{tag.m_instances[index]};
    abc::ConstantPoolInfo const &constant_pool_info{tag.m_constant_pool_info};

    this->m_class = DofusType(instance_info.get_name(constant_pool_info));
    this->m_super = DofusType(instance_info.get_super_name(constant_pool_info));
    this->extract_fields(constant_pool_info, class_info, instance_info);
    this->extract_getters_setters(constant_pool_info, instance_info);
    this->extract_methods(tag, index);
}

void ClassInformation::extract_fields(abc::ConstantPoolInfo const &constant_pool_info,
                                      abc::ClassInfo const &class_info,
                                      abc::InstanceInfo const &instance_info) {

    for (auto const &trait : class_info.m_traits) {
        this->extract_field_trait(trait, constant_pool_info);
    }
    for (auto const &trait : instance_info.m_traits) {
        this->extract_field_trait(trait, constant_pool_info);
    }
}

void ClassInformation::extract_field_trait(abc::TraitInfo const &trait,
                                           abc::ConstantPoolInfo const &constant_pool_info) {
    abc::TraitKind const kind{trait.get_kind()};
    if (kind == abc::TraitKind::SLOT || kind == abc::TraitKind::CONST) {
        std::string const field_name{trait.get_name(constant_pool_info)};
        if (field_name == "protocolId") {
            this->m_protocol_id = std::stoul(trait.get_data(constant_pool_info));
        } else {
            ClassField field;
            field.name          = DofusAttribute(field_name);
            field.type          = DofusType(trait.get_type(constant_pool_info));
            field.default_value = trait.get_data(constant_pool_info);
            m_fields[field.name.get_attribute_name()] = field;
        }
    }
}

void ClassInformation::extract_getters_setters(abc::ConstantPoolInfo const &constant_pool_info,
                                               abc::InstanceInfo const &instance_info) {

    std::unordered_set<std::string> found_either_getter_or_setter;

    for (auto const &trait : instance_info.m_traits) {
        if (trait.is_getter() || trait.is_setter()) {
            std::string fictive_property_name{trait.get_name(constant_pool_info)};
            if (found_either_getter_or_setter.find(fictive_property_name) !=
                found_either_getter_or_setter.end()) {
                // We found the 2 getter/setter
                // Add a fictive property
                std::string const old_property_name{"_" + fictive_property_name};
                m_fields[fictive_property_name] = m_fields.at(old_property_name);
                m_fields[fictive_property_name].name.set_attribute_name(old_property_name);
            } else {
                found_either_getter_or_setter.emplace(std::move(fictive_property_name));
            }
        }
    }
}

void ClassInformation::extract_methods(TagDoAbc const &tag, size_t index) {

    for (auto const &trait : tag.m_instances[index].m_traits) {
        this->extract_method(tag, trait);
    }
    for (auto const &trait : tag.m_classes[index].m_traits) {
        this->extract_method(tag, trait);
    }

}

void ClassInformation::extract_method(TagDoAbc const &tag, abc::TraitInfo const &trait) {

    if (!trait.is_method()) { return; }
    std::string const trait_name{trait.get_name(tag.m_constant_pool_info)};
    if (starts_with(trait_name, "serializeAs_")) {
        uint32 const          method_index{trait.get_index()};
        abc::MethodBody const &method_body{tag.get_method_body(method_index)};
        DofusMethod const     method(trait.name(tag));

        PatternMatcher matcher(tag.m_constant_pool_info, method, method_body);
        matcher.match_all(m_fields);
    } else if (trait_name == "pack") {
        uint32 const             method_index{trait.get_index()};
        abc::MethodBody const    &method_body{tag.get_method_body(method_index)};
        std::vector<Instr> const instructions{method_body.disassemble()};
        for (Instr const         &instruction : instructions) {
            if (instruction.model.name == "callpropvoid") {
                DofusMethod const method(tag.m_constant_pool_info
                                            .get_multiname(instruction.operands[0])
                                            .to_string(tag.m_constant_pool_info));
                this->m_use_hash_function |= method.get_method_name() == "HASH_FUNCTION";
            }
        }
    }
}

json ClassInformation::to_json(boost::optional<json> const &field_default_values) const {
    json j;
    j["name"]       = this->m_class.get_type();
    j["namespace"]  = this->m_class.get_namespace().to_string();
    if (this->m_super.to_string() != "Object") {
        j["super"]          = this->m_super.get_type();
        j["supernamespace"] = this->m_super.get_namespace().to_string();
    } else {
        j["super"]          = "";
        j["supernamespace"] = "";
    }
    j["protocolID"] = this->m_protocol_id;
    j["fields"]     = json::array();
    for (auto const &[field_name, field] : m_fields) {
        j["fields"].push_back(field.to_json(field_default_values));
    }
    j["use_hash_function"] = this->m_use_hash_function;
    return j;
}
