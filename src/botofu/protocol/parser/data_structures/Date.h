#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DATE_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DATE_H

#include <cstddef>

#include <nlohmann/json.hpp>

using json = nlohmann::json;

struct Date {

    std::size_t year;
    std::size_t month;
    std::size_t day;
    std::size_t hour;
    std::size_t minute;

};

void to_json(json &j, Date const &date);

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DATE_H
