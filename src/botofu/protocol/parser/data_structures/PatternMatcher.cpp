#include "PatternMatcher.h"

#include <utility>

#include "botofu/protocol/parser/matchers/composed/UnmatchedWriteMatcher.h"
#include "botofu/protocol/parser/matchers/composed/AttributeCallSerializeMethodMatcher.h"
#include "botofu/protocol/parser/matchers/composed/AttributeWriteMatcher.h"
#include "botofu/protocol/parser/matchers/composed/AttributeWriteBothBoundChecksMatcher.h"
#include "botofu/protocol/parser/matchers/composed/AttributeWriteLowerBoundCheck.h"
#include "botofu/protocol/parser/matchers/composed/BooleanByteWrapperMatcher.h"
#include "botofu/protocol/parser/matchers/composed/BooleanByteWrapperWriteByteMatcher.h"
#include "botofu/protocol/parser/matchers/composed/SuperSerializeAsMatcher.h"
#include "botofu/protocol/parser/matchers/composed/RuntimeLenListTypeManagerWriteMatcher.h"
#include "botofu/protocol/parser/matchers/composed/RuntimeLenListSerializeAsMatcher.h"
#include "botofu/protocol/parser/matchers/composed/RuntimeLenListBoundCheckMatcher.h"
#include "botofu/protocol/parser/matchers/composed/RuntimeLenListNumericType.h"
#include "botofu/protocol/parser/matchers/composed/CheckIfNullSerializeAsMatcher.h"
#include "botofu/protocol/parser/matchers/composed/ForLoopConstantSizeMatcher.h"
#include "botofu/protocol/parser/matchers/composed/RuntimeLenListBothBoundsCheckMatcher.h"
#include "botofu/protocol/parser/matchers/composed/AttributeSerializeWithTypeIdMatcher.h"
#include "botofu/protocol/parser/matchers/composed/ForLoopConstantSizeSerializeAsMatcher.h"

void PatternMatcher::add_matcher(const std::shared_ptr<BaseComposedMatcher> &matcher) {
    std::size_t const first_occurence{matcher->find_pattern_and_update_matchers(m_code,
                                                                                m_position)};
    m_matchers.emplace(first_occurence, matcher);
}

PatternMatcher::PatternMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                               const DofusMethod &method,
                               abc::MethodBody const &method_body)
        : m_code(method_body.disassemble()),
          m_matchers(),
          m_position{0} {

    #define BOTOFU_ADD_MATCHER(type) this->add_matcher(std::make_shared< type >(constant_pool_info, method))
    BOTOFU_ADD_MATCHER(AttributeCallSerializeMethodMatcher);
    BOTOFU_ADD_MATCHER(AttributeSerializeWithTypeIdMatcher);
    BOTOFU_ADD_MATCHER(AttributeWriteBothBoundChecksMatcher);
    BOTOFU_ADD_MATCHER(AttributeWriteLowerBoundCheck);
    BOTOFU_ADD_MATCHER(AttributeWriteMatcher);
    BOTOFU_ADD_MATCHER(BooleanByteWrapperMatcher);
    BOTOFU_ADD_MATCHER(BooleanByteWrapperWriteByteMatcher);
    BOTOFU_ADD_MATCHER(CheckIfNullSerializeAsMatcher);
    BOTOFU_ADD_MATCHER(ForLoopConstantSizeMatcher);
    BOTOFU_ADD_MATCHER(ForLoopConstantSizeSerializeAsMatcher);
    BOTOFU_ADD_MATCHER(RuntimeLenListBothBoundsCheckMatcher);
    BOTOFU_ADD_MATCHER(RuntimeLenListBoundCheckMatcher);
    BOTOFU_ADD_MATCHER(RuntimeLenListNumericType);
    BOTOFU_ADD_MATCHER(RuntimeLenListSerializeAsMatcher);
    BOTOFU_ADD_MATCHER(RuntimeLenListTypeManagerWriteMatcher);
    BOTOFU_ADD_MATCHER(SuperSerializeAsMatcher);
    BOTOFU_ADD_MATCHER(UnmatchedWriteMatcher);
    #undef BOTOFU_ADD_MATCHER
}

void PatternMatcher::match_all(BaseComposedMatcher::FieldsType &fields) {
    // Get the best match and its position
    auto best_match = m_matchers.top();
    m_position = best_match.first;
    // While there is at least one positive match
    while (m_position < m_code.size()) {
        auto const &matcher = best_match.second;
        // Call the matcher's update_fields function.
        matcher->update_fields(fields);
        // Update the position to the end of the matched instruction sequence.
        m_position += matcher->size();
        // Refresh the matchers that point to instructions we already covered.
        this->refresh_matchers();
        // Refresh the best match.
        best_match = m_matchers.top();
        // Refresh the position.
        m_position = best_match.first;
    }
}

void PatternMatcher::refresh_matchers() {
    auto best_match = m_matchers.top();
    while (best_match.first < m_position) {
        // Pop the invalid matcher.
        m_matchers.pop();
        // And push it again to update it.
        this->add_matcher(best_match.second);
        // And loop over best_match
        best_match = m_matchers.top();
    }
}

template <typename MatcherType, typename... Args>
void PatternMatcher::add_matcher(Args... args) {
    this->add_matcher(std::make_shared<MatcherType>(std::forward(args)...));
}

bool
BaseComposedMatcherComparator::operator()(ComparedType const &lhs, ComparedType const &rhs) const {
    /*
     * We want the earliest match (smallest index in .first) to be on top of the queue, i.e. to
     * pop first. In case of equality, the longest sequence matched should pop first.
     *
     * We return true if lhs should come after rhs in pops, otherwise false.
     */
    // First sort by match index
    if (lhs.first != rhs.first) { return lhs.first > rhs.first; }
    // If 2 matchers matched at the same position, the longest pops first.
    return lhs.second->size() < rhs.second->size();
}
