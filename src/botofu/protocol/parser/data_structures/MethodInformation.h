#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_METHODINFORMATION_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_METHODINFORMATION_H

#include <vector>
#include <string>

#include "botofu/swf/parser/tags/TagDoAbc.h"

namespace {

    enum Type {
        BOOL, INT8, UINT8, INT16, UINT16, INT32, UINT32, INT64, UINT64, FLOAT, DOUBLE
    };

    struct Field {
        Type        type_read;
        bool        is_read_var;
        bool        is_vector;
        bool        is_dynamic_length;
        std::size_t fixed_length;
        Type        length_type;
        bool        use_type_manager;
        std::string name;
    };
}

class MethodInformation {

public:

    MethodInformation(TagDoAbc const &tag, uint32 index);

private:
    std::vector<Field> m_fields;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_METHODINFORMATION_H
