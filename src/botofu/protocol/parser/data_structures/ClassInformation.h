#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSINFORMATION_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSINFORMATION_H

#include <string>
#include <unordered_map>
#include <vector>

#include <nlohmann/json.hpp>
#include <boost/optional.hpp>

#include "botofu/ios/types.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/swf/parser/abc/ClassInfo.h"
#include "botofu/swf/parser/abc/InstanceInfo.h"
#include "botofu/protocol/parser/dofus_interface/DofusType.h"
#include "botofu/protocol/parser/dofus_interface/DofusAttribute.h"
#include "botofu/protocol/parser/data_structures/ClassField.h"

using json = nlohmann::json;

class ClassInformation {

public:
    ClassInformation(TagDoAbc const &tag, std::size_t index);

    /**
     * @brief Export the ClassInformation instance to JSON.
     * @param field_default_values optional default values for the fields serialisation. If
     *     provided, the JSON serialisation of fields will only contain the values that are
     *     different from the provided defaults. If not provided (i.e. boost::none), the JSON
     *     serialisation of fields will contain all the entries, even empty ones.
     * @return the JSON serialisation of the instance.
     */
    [[nodiscard]] json
    to_json(boost::optional<json> const &field_default_values = boost::none) const;

private:

    void extract_fields(abc::ConstantPoolInfo const &constant_pool_info,
                        abc::ClassInfo const &class_info,
                        abc::InstanceInfo const &instance_info);

    void extract_field_trait(abc::TraitInfo const &trait,
                             abc::ConstantPoolInfo const &constant_pool_info);

    void extract_getters_setters(abc::ConstantPoolInfo const &constant_pool_info,
                                 abc::InstanceInfo const &instance_info);

    void extract_methods(TagDoAbc const &tag, size_t index);

    void extract_method(TagDoAbc const &tag, abc::TraitInfo const &trait);

    std::unordered_map<std::string /*field name*/, ClassField> m_fields;
    DofusType                                                  m_class;
    DofusType                                                  m_super;
    unsigned long                                              m_protocol_id;
    bool                                                       m_use_hash_function;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSINFORMATION_H
