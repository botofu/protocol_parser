#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSFIELD_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSFIELD_H

#include <string>
#include <nlohmann/json.hpp>
#include <boost/optional.hpp>

#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"
#include "botofu/protocol/parser/dofus_interface/DofusType.h"
#include "botofu/protocol/parser/dofus_interface/DofusAttribute.h"
#include "botofu/protocol/parser/dofus_interface/Limits.h"
#include "botofu/protocol/parser/matchers/instructions/io.h"

using json = nlohmann::json;

struct ClassField {

    ClassField();

    DofusAttribute name;
    DofusType      type;
    std::string    default_value;

    DofusMethod self_serialize_method;
    DofusMethod write_type_id_method;
    DofusMethod write_method;
    DofusMethod write_length_method;
    DofusMethod write_false_if_null_method;
    Limits      bounds;
    int         boolean_byte_wrapper_position;
    std::size_t constant_length;
    bool        prefixed_by_type_id;
    bool        use_boolean_byte_wrapper;
    bool        null_checked;

    /**
     * @brief Export the ClassField instance to JSON.
     *
     * If @p default_values is provided (i.e. not boost::none), only include in the resulting JSON
     * the fields that are different than the default value.
     *
     * @param default_values
     * @return
     */
    [[nodiscard]] json
    to_json(boost::optional<json> const &default_values = boost::none) const;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_CLASSFIELD_H
