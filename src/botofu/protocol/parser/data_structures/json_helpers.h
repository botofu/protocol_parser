#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_JSON_HELPERS_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_JSON_HELPERS_H

#include <boost/optional.hpp>
#include <nlohmann/json.hpp>

#include "botofu/protocol/parser/data_structures/ClassField.h"

using json = nlohmann::json;

template <typename T>
void insert_value(boost::optional<json> const &default_values,
                  json &j,
                  std::string const &key,
                  T const &value) {
    // If default_value is not provided, just include it.
    if (!default_values) {
        j[key] = value;
        return;
    }
    // Else, if the default value is not provided, insert
    if (default_values->find(key) == default_values->end()) {
        j[key] = value;
        return;
    }
    // Else, the default value is provided and so we need to check if we should insert
    // our value or not
    if (value != (*default_values)[key]) {
        j[key] = value;
        return;
    }
}

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_JSON_HELPERS_H
