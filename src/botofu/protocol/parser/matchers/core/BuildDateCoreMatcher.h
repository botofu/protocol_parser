#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_BUILDDATECOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_BUILDDATECOREMATCHER_H

#include <string>

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/data_structures/Date.h"

struct BuildDateCoreMatcher final : public BaseCoreMatcher {

    explicit BuildDateCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions, std::size_t match_start) final;

    Date date;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_BUILDDATECOREMATCHER_H
