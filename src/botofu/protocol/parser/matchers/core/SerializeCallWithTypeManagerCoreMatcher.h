#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_SERIALIZECALLWITHTYPEMANAGERCOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_SERIALIZECALLWITHTYPEMANAGERCOREMATCHER_H

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/dofus_interface/DofusAttribute.h"
#include "botofu/protocol/parser/matchers/instructions/io.h"

struct SerializeCallWithTypeManagerCoreMatcher final : public BaseCoreMatcher {

    explicit SerializeCallWithTypeManagerCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions, std::size_t match_start) final;

    DofusAttribute list_attribute;
    DofusType      type;
    DofusMethod    method;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_SERIALIZECALLWITHTYPEMANAGERCOREMATCHER_H
