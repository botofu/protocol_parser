#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_WRITEATTRIBUTEATTRIBUTEMETHODCOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_WRITEATTRIBUTEATTRIBUTEMETHODCOREMATCHER_H

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/dofus_interface/DofusAttribute.h"
#include "botofu/protocol/parser/matchers/instructions/io.h"

struct WriteAttributeAttributeMethodCoreMatcher final : public BaseCoreMatcher {

    explicit WriteAttributeAttributeMethodCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions, std::size_t match_start) final;

    DofusAttribute attribute;
    DofusAttribute subattribute;
    DofusMethod    method;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_WRITEATTRIBUTEATTRIBUTEMETHODCOREMATCHER_H
