#include "SuperSerializeAsCoreMatcher.h"

#include <boost/algorithm/string/predicate.hpp>

using boost::algorithm::starts_with;

SuperSerializeAsCoreMatcher::SuperSerializeAsCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"getlocal_0", "getlocal(_[[:digit:]])?", "callsupervoid"},
                          constant_pool_info) {

}

void SuperSerializeAsCoreMatcher::update(std::vector<Instr> const &instructions,
                                         std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    Instr const       &callsupervoid_instruction{instructions[match_start + 2]};
    DofusMethod const callsupervoid_method(p_constant_pool_info.get_multiname(
            callsupervoid_instruction.operands[0]).to_string(p_constant_pool_info));
    std::string const callsupervoid_method_name(callsupervoid_method.get_method_name());
    if (starts_with(callsupervoid_method_name, "serializeAs")) {
        this->method = callsupervoid_method;
    }
}
