#include "SingleCallPropVoidCoreMatcher.h"
#include <boost/algorithm/string/predicate.hpp>
#include "botofu/protocol/parser/matchers/instructions/io.h"
#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"

using boost::algorithm::starts_with;

SingleCallPropVoidCoreMatcher::SingleCallPropVoidCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"callpropvoid"}, constant_pool_info),
          is_valid(false),
          called_method() {

}

void
SingleCallPropVoidCoreMatcher::update(std::vector<Instr> const &instructions,
                                      std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    Instr const       &propvoid_instruction{instructions[match_start]};
    DofusMethod const propvoid_method(p_constant_pool_info.get_multiname(
            propvoid_instruction.operands[0]).to_string(p_constant_pool_info));
    std::string const propvoid_method_name(propvoid_method.get_method_name());
    if (starts_with(propvoid_method_name, "write")) {
        IOMethodType const write_type{get_io_method_type_from_name(propvoid_method_name)};
        this->is_valid      = (write_type != IOMethodType::UNKNOWN);
        this->called_method = propvoid_method;
    } else if (starts_with(propvoid_method_name, "serialize")) {
        this->is_valid      = true;
        this->called_method = propvoid_method;
    }
}
