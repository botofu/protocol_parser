#include "ComparisonLessThanArrayIndexingCoreMatcher.h"

ComparisonLessThanArrayIndexingCoreMatcher::ComparisonLessThanArrayIndexingCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"getlocal_0",
                           "getproperty",
                           "getlocal(_[[:digit:]])?",
                           "getproperty",
                           push_instruction_regex,
                           "(ifnlt)|(lessthan)"},
                          constant_pool_info),
          attribute{ },
          limits{ },
          push_type{ } {

}

void ComparisonLessThanArrayIndexingCoreMatcher::update(std::vector<Instr> const &instructions,
                                                        std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    // First extract the attribute.
    this->attribute = this->get_attribute_from_getproperty(instructions[match_start + 1]);

    // Then the compared value.
    Instr const &push_instruction{instructions[match_start + 4]};
    this->push_type    = get_push_type_from_name(push_instruction.model.name);
    this->limits.lower = get_push_representation_from_type(this->push_type,
                                                           p_constant_pool_info,
                                                           push_instruction);
}
