#include "VersionCoreMatcher.h"

#include <regex>
#include <spdlog/spdlog.h>

VersionCoreMatcher::VersionCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"getlex", "findpropstrict", "pushstring"}, constant_pool_info),
          major{0},
          minor{0},
          patch{0},
          build_type{BuildType::UNKNOWN} {

}

void VersionCoreMatcher::update(std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    Instr const       &pushstring_instruction{instructions[match_start + 2]};
    std::string const version(p_constant_pool_info.get_string_info(pushstring_instruction.operands[0])
                                                  .to_string());
    // version should be "[major].[patch].[patch]-[build_type]]
    std::regex        version_regex(
            "^([[:digit:]]+)\\.([[:digit:]]+)\\.([[:digit:]]+)-([[:alpha:]]+)",
            std::regex_constants::extended);
    std::smatch       version_match;
    if (!std::regex_search(version, version_match, version_regex)) {
        SPDLOG_WARN("Dofus version could not be retrieved. "
                    "The version '0.0.0-release' will be used instead.");
        return;
    }
    this->major = std::stoul(version_match[1]);
    this->minor = std::stoul(version_match[2]);
    this->patch = std::stoul(version_match[3]);
    if (version_match[4] == "release") { this->build_type = BuildType::RELEASE; }
    else {
        SPDLOG_WARN("Found a build type not matching 'release': '{}'.", version_match[4].str());
        this->build_type = BuildType::UNKNOWN;
    }
}
