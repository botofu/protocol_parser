#include "BaseCoreMatcher.h"

BaseCoreMatcher::BaseCoreMatcher(std::vector<std::string> const &regexes,
                                 abc::ConstantPoolInfo const &constant_pool_info,
                                 std::regex_constants::syntax_option_type flag)
        : p_constant_pool_info(constant_pool_info),
          p_match_instruction_index{0} {
    for (std::string const &regex : regexes) {
        m_regexes.emplace_back(regex, flag);
    }
}

std::size_t BaseCoreMatcher::size() const {
    return m_regexes.size();
}

bool
BaseCoreMatcher::match_pattern(std::vector<Instr> const &instructions, std::size_t start) const {
    for (std::size_t i{0}; i < m_regexes.size(); ++i) {
        std::string const &instruction_name{instructions[start + i].model.name};
        if (!std::regex_match(instruction_name, m_regexes[i])) {
            return false;
        }
    }
    return true;
}

std::size_t BaseCoreMatcher::find_pattern_and_update(std::vector<Instr> const &instructions,
                                                     std::size_t start) {
    std::size_t const start_match{this->find_pattern(instructions, start)};
    if (start_match < instructions.size()) {
        this->update(instructions, start_match);
    }
    return start_match;
}

std::size_t
BaseCoreMatcher::find_pattern(std::vector<Instr> const &instructions, std::size_t start) const {
    if (instructions.size() <= m_regexes.size()) { return instructions.size(); }

    std::size_t const max_start_search{instructions.size() - m_regexes.size()};
    for (std::size_t  i{start}; i < max_start_search; ++i) {
        if (this->match_pattern(instructions, i)) {
            return i;
        }
    }
    return instructions.size();
}

DofusAttribute BaseCoreMatcher::get_attribute_from_getproperty(Instr const &instruction) const {
    uint32 const attribute_index{instruction.operands[0]};
    return DofusAttribute(p_constant_pool_info.get_multiname(attribute_index)
                                              .to_string(p_constant_pool_info));
}

DofusMethod BaseCoreMatcher::get_method_from_callprop_void(Instr const &instruction) const {
    uint32 const method_name_index{instruction.operands[0]};
    return DofusMethod(p_constant_pool_info.get_multiname(method_name_index)
                                           .to_string(p_constant_pool_info));
}

void BaseCoreMatcher::update([[maybe_unused]] std::vector<Instr> const &instructions,
                             std::size_t match_start) {
    p_match_instruction_index = match_start;
}

std::size_t BaseCoreMatcher::get_match_instruction_index() const {
    return p_match_instruction_index;
}
