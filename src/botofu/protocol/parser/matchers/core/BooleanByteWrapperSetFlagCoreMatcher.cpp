#include "BooleanByteWrapperSetFlagCoreMatcher.h"

#include "botofu/protocol/parser/matchers/instructions/push.h"

BooleanByteWrapperSetFlagCoreMatcher::BooleanByteWrapperSetFlagCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"getlex",
                           "getlocal(_[[:digit:]])?",
                           push_instruction_regex,
                           "getlocal_0",
                           "getproperty",
                           "callproperty",
                           "convert_u",
                           "setlocal(_[[:digit:]])?"}, constant_pool_info),
          bit_index{0},
          attribute{ } {

}

void
BooleanByteWrapperSetFlagCoreMatcher::update(std::vector<Instr> const &instructions,
                                             std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    Instr const               &bit_push_instruction{instructions[match_start + 2]};
    PushInstructionType const bit_push_type{get_push_type_from_name(bit_push_instruction.model
                                                                                        .name)};
    std::string const         bit_value_representation{get_push_representation_from_type(
            bit_push_type,
            p_constant_pool_info,
            bit_push_instruction)};
    this->bit_index = std::stoul(bit_value_representation);

    Instr const &getproperty_instruction{instructions[match_start + 4]};
    this->attribute = this->get_attribute_from_getproperty(getproperty_instruction);
}
