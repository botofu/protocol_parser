#include "ForLoopStartCoreMatcher.h"

ForLoopStartCoreMatcher::ForLoopStartCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({push_instruction_regex,
                           "convert_u",
                           "setlocal(_[[:digit:]])?",
                           "jump",
                           "label"},
                          constant_pool_info),
          push_type{ },
          limits{ } {

}

void
ForLoopStartCoreMatcher::update(std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    // Update the push type and limit
    Instr const &push_instruction{instructions[match_start]};
    this->push_type    = get_push_type_from_name(push_instruction.model.name);
    this->limits.lower = get_push_representation_from_type(this->push_type,
                                                           p_constant_pool_info,
                                                           push_instruction);
}
