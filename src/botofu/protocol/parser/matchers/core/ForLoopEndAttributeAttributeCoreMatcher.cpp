#include "ForLoopEndAttributeAttributeCoreMatcher.h"

ForLoopEndAttributeAttributeCoreMatcher::ForLoopEndAttributeAttributeCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"getlocal(_[[:digit:]])?",
                           "increment",
                           "convert_u",
                           "setlocal(_[[:digit:]])?",
                           "getlocal(_[[:digit:]])?",
                           "getlocal_0",
                           "getproperty",
                           "getproperty",
                           "iflt"},
                          constant_pool_info),
          attribute{ },
          subattribute{ } {

}

void ForLoopEndAttributeAttributeCoreMatcher::update(std::vector<Instr> const &instructions,
                                                     std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    // First recover the name of the first attribute
    this->attribute = this->get_attribute_from_getproperty(instructions[match_start + 6]);

    // Then recover the attribute that is serialized (likely to be the length but we stay generic)
    this->subattribute = this->get_attribute_from_getproperty(instructions[match_start + 7]);

}
