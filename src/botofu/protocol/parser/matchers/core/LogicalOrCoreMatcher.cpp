#include "LogicalOrCoreMatcher.h"

LogicalOrCoreMatcher::LogicalOrCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"dup", "iftrue", "pop"}, constant_pool_info) {

}

void LogicalOrCoreMatcher::update(std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    // Do nothing here, we do not have anything to update.
}
