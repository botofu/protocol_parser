#include "BuildDateCoreMatcher.h"

#include <regex>
#include <spdlog/spdlog.h>

BuildDateCoreMatcher::BuildDateCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"getlex", "pushstring", "setproperty"}, constant_pool_info),
          date{ } {

}

void BuildDateCoreMatcher::update(std::vector<Instr> const &instructions, std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    Instr const       &pushstring_instruction{instructions[match_start + 1]};
    std::string const date_str = p_constant_pool_info.get_string_info(pushstring_instruction.operands[0])
                                                     .to_string();
    std::regex        date_regex(
            "([[:digit:]]+)-([[:digit:]]+)-([[:digit:]]+) ([[:digit:]]+):([[:digit:]]+)",
            std::regex_constants::extended);
    std::smatch       date_match;
    if (!std::regex_search(date_str, date_match, date_regex)) {
        SPDLOG_WARN("Client build date could not be retrieved from the string '{}'. "
                    "A date filled with zeros will be used.", date_str);
        return;
    }
    this->date.year   = std::stoul(date_match[3]);
    this->date.month  = std::stoul(date_match[2]);
    this->date.day    = std::stoul(date_match[1]);
    this->date.hour   = std::stoul(date_match[4]);
    this->date.minute = std::stoul(date_match[5]);

}
