#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_BASECOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_BASECOREMATCHER_H

#include <vector>
#include <string>
#include <regex>

#include "botofu/swf/parser/abc/MethodBody/AVM2Instructions.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"
#include "botofu/protocol/parser/data_structures/ClassField.h"
#include "botofu/protocol/parser/dofus_interface/DofusAttribute.h"
#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"

struct BaseCoreMatcher {

    BaseCoreMatcher() = delete;

    virtual ~BaseCoreMatcher() = default;

    BaseCoreMatcher(std::vector<std::string> const &regexes,
                    abc::ConstantPoolInfo const &constant_pool_info,
                    std::regex_constants::syntax_option_type flag = std::regex_constants::optimize |
                                                                    std::regex_constants::extended);

    /**
     * @brief Find the pattern stored by the instance in the given sequence of instructions.
     * @param instructions the list of instructions we want to search in.
     * @param start index of the first considered instruction in @p instruction.
     * @return the index of the first instruction matching the pattern or instruction.size() if the
     *         pattern could not be found in the instruction list.
     */
    [[nodiscard]] std::size_t
    find_pattern(std::vector<Instr> const &instructions, std::size_t start = 0) const;

    /**
     * @brief Find the pattern stored by the instance in the given sequence of instructions and
     *        call the virtual update method.
     * @param instructions the list of instructions we want to search in.
     * @param start index of the first considered instruction in @p instruction.
     * @return the index of the first instruction matching the pattern or instruction.size() if the
     *         pattern could not be found in the instruction list.
     */
    [[nodiscard]] std::size_t
    find_pattern_and_update(std::vector<Instr> const &instructions, std::size_t start = 0);

    /**
     * @brief Check if the instructions starting at @p start match the stored pattern.
     * @param instructions the list of instructions we want to search in.
     * @param start index of the first considered instruction in @p instruction.
     * @return true if instructions[start] and the following instructions match the provided
     *         pattern, else false.
     */
    [[nodiscard]] bool
    match_pattern(std::vector<Instr> const &instructions, std::size_t start) const;

    /**
     * @brief Returns the length of the stored pattern.
     * @return the length of the stored pattern.
     */
    [[nodiscard]] std::size_t size() const;

    virtual void update(std::vector<Instr> const &instructions, std::size_t match_start);

    [[nodiscard]] std::size_t get_match_instruction_index() const;

protected:
    abc::ConstantPoolInfo const &p_constant_pool_info;
    std::size_t                 p_match_instruction_index;

    [[nodiscard]] DofusAttribute get_attribute_from_getproperty(Instr const &instruction) const;

    [[nodiscard]] DofusMethod get_method_from_callprop_void(Instr const &instruction) const;

private:

    std::vector<std::regex> m_regexes;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_BASECOREMATCHER_H
