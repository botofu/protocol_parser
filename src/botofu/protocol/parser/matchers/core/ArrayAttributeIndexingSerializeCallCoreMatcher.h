#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_ARRAYATTRIBUTEINDEXINGSERIALIZECALLCOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_ARRAYATTRIBUTEINDEXINGSERIALIZECALLCOREMATCHER_H

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/dofus_interface/DofusAttribute.h"

struct ArrayAttributeIndexingSerializeCallCoreMatcher final : public BaseCoreMatcher {

    explicit ArrayAttributeIndexingSerializeCallCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions, std::size_t match_start) final;

    DofusAttribute attribute;
    DofusMethod    method;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_ARRAYATTRIBUTEINDEXINGSERIALIZECALLCOREMATCHER_H
