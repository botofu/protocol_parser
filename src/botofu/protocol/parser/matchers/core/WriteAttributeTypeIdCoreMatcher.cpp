#include "WriteAttributeTypeIdCoreMatcher.h"

WriteAttributeTypeIdCoreMatcher::WriteAttributeTypeIdCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"getlocal(_[[:digit:]])?",
                           "getlocal_0",
                           "getproperty",
                           "callproperty",
                           "callpropvoid"}, constant_pool_info),
          attribute{ } {

}

void WriteAttributeTypeIdCoreMatcher::update(std::vector<Instr> const &instructions,
                                             std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    this->attribute = this->get_attribute_from_getproperty(instructions[match_start + 3]);
    this->method    = this->get_method_from_callprop_void(instructions[match_start + 4]);
}
