#include "IfFalseInstructionCoreMatcher.h"

IfFalseInstructionCoreMatcher::IfFalseInstructionCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"iffalse"}, constant_pool_info) {

}

void IfFalseInstructionCoreMatcher::update(std::vector<Instr> const &instructions,
                                           std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
}
