#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_COMPARISONLESSTHANARRAYINDEXINGCOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_COMPARISONLESSTHANARRAYINDEXINGCOREMATCHER_H

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/dofus_interface/DofusAttribute.h"
#include "botofu/protocol/parser/dofus_interface/Limits.h"
#include "botofu/protocol/parser/matchers/instructions/push.h"

struct ComparisonLessThanArrayIndexingCoreMatcher final : public BaseCoreMatcher {

    explicit ComparisonLessThanArrayIndexingCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions, std::size_t match_start) final;

    DofusAttribute      attribute;
    Limits              limits;
    PushInstructionType push_type;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_COMPARISONLESSTHANARRAYINDEXINGCOREMATCHER_H
