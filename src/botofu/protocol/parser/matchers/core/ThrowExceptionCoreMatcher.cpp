#include "ThrowExceptionCoreMatcher.h"

#include "botofu/protocol/parser/matchers/instructions/push.h"

ThrowExceptionCoreMatcher::ThrowExceptionCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"findpropstrict", "pushstring", "getlocal_0",
                           "getproperty", "add", "pushstring", "add", "constructprop", "throw"},
                          constant_pool_info) {

}

void
ThrowExceptionCoreMatcher::update(std::vector<Instr> const &instructions,
                                  std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    // Do nothing here, we do not have anything to update.
}
