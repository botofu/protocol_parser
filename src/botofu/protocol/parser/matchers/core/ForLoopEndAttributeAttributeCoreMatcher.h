#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_FORLOOPENDATTRIBUTEATTRIBUTECOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_FORLOOPENDATTRIBUTEATTRIBUTECOREMATCHER_H

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/matchers/instructions/push.h"
#include "botofu/protocol/parser/dofus_interface/Limits.h"
#include "botofu/protocol/parser/dofus_interface/DofusAttribute.h"

struct ForLoopEndAttributeAttributeCoreMatcher final : public BaseCoreMatcher {

    explicit ForLoopEndAttributeAttributeCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions, std::size_t match_start) final;

    DofusAttribute attribute;
    DofusAttribute subattribute;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_FORLOOPENDATTRIBUTEATTRIBUTECOREMATCHER_H
