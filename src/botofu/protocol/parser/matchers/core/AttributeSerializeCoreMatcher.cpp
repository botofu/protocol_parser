#include "AttributeSerializeCoreMatcher.h"

AttributeSerializeCoreMatcher::AttributeSerializeCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"getlocal_0", "getproperty", "getlocal(_[[:digit:]])?", "callpropvoid"},
                          constant_pool_info) {

}

void
AttributeSerializeCoreMatcher::update(std::vector<Instr> const &instructions,
                                      std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    this->attribute = this->get_attribute_from_getproperty(instructions[match_start + 1]);
    this->method    = this->get_method_from_callprop_void(instructions[match_start + 3]);
}
