#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_CALLSUPERVOIDCOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_CALLSUPERVOIDCOREMATCHER_H

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"

struct CallSuperVoidCoreMatcher final : public BaseCoreMatcher {

    explicit CallSuperVoidCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions, std::size_t match_start) final;

    DofusMethod called_method;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_CALLSUPERVOIDCOREMATCHER_H
