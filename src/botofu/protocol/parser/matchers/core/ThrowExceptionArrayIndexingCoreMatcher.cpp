#include "ThrowExceptionArrayIndexingCoreMatcher.h"

ThrowExceptionArrayIndexingCoreMatcher::ThrowExceptionArrayIndexingCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"findpropstrict",
                           "pushstring",
                           "getlocal_0",
                           "getproperty",
                           "getlocal(_[[:digit:]])?",
                           "getproperty",
                           "add",
                           "pushstring",
                           "add",
                           "constructprop",
                           "throw"},
                          constant_pool_info) {

}

void ThrowExceptionArrayIndexingCoreMatcher::update(std::vector<Instr> const &instructions,
                                                    std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    // Do nothing here.
}
