#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_FORLOOPSTARTCOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_FORLOOPSTARTCOREMATCHER_H

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/dofus_interface/Limits.h"
#include "botofu/protocol/parser/matchers/instructions/push.h"

struct ForLoopStartCoreMatcher final : public BaseCoreMatcher {

    explicit ForLoopStartCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions, std::size_t match_start) final;

    PushInstructionType push_type;
    Limits              limits;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_FORLOOPSTARTCOREMATCHER_H
