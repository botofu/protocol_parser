#include "CheckIfNullSerializeAsCoreMatcher.h"

CheckIfNullSerializeAsCoreMatcher::CheckIfNullSerializeAsCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info)
        : BaseCoreMatcher({"getlocal_0",
                           "getproperty",
                           "pushnull",
                           "ifne",
                           "debugline",
                           "getlocal(_[[:digit:]])?",
                           push_instruction_regex,
                           "callpropvoid",
                           "jump",
                           "debugline",
                           "getlocal(_[[:digit:]])?",
                           push_instruction_regex,
                           "callpropvoid",
                           "debugline",
                           "getlocal_0",
                           "getproperty",
                           "getlocal(_[[:digit:]])?",
                           "callpropvoid"}, constant_pool_info) {

}

void CheckIfNullSerializeAsCoreMatcher::update(std::vector<Instr> const &instructions,
                                               std::size_t match_start) {
    BaseCoreMatcher::update(instructions, match_start);
    this->attribute            = this->get_attribute_from_getproperty(instructions[match_start +
                                                                                   1]);
    this->method               = this->get_method_from_callprop_void(instructions[match_start +
                                                                                  17]);
    this->is_null_write_method = this->get_method_from_callprop_void(instructions[match_start + 7]);
}
