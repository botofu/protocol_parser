#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_FORLOOPENDCONSTANTSIZECOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_FORLOOPENDCONSTANTSIZECOREMATCHER_H

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/matchers/instructions/push.h"
#include "botofu/protocol/parser/dofus_interface/Limits.h"
#include "botofu/protocol/parser/dofus_interface/DofusAttribute.h"

struct ForLoopEndConstantSizeCoreMatcher final : public BaseCoreMatcher {

    explicit ForLoopEndConstantSizeCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions, std::size_t match_start) final;

    std::size_t loop_size;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_FORLOOPENDCONSTANTSIZECOREMATCHER_H
