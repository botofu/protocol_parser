#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_IFFALSEINSTRUCTIONCOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_IFFALSEINSTRUCTIONCOREMATCHER_H

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"

struct IfFalseInstructionCoreMatcher final : public BaseCoreMatcher {

    explicit IfFalseInstructionCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions, std::size_t match_start) final;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_IFFALSEINSTRUCTIONCOREMATCHER_H
