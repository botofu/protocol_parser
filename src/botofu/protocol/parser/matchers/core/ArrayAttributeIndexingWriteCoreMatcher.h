#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_ARRAYATTRIBUTEINDEXINGWRITECOREMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_ARRAYATTRIBUTEINDEXINGWRITECOREMATCHER_H

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/dofus_interface/DofusAttribute.h"

struct ArrayAttributeIndexingWriteCoreMatcher final : public BaseCoreMatcher {

    explicit ArrayAttributeIndexingWriteCoreMatcher(abc::ConstantPoolInfo const &constant_pool_info);

    void update(std::vector<Instr> const &instructions, std::size_t match_start) final;

    DofusAttribute attribute;
    DofusMethod    write_method;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_CORE_ARRAYATTRIBUTEINDEXINGWRITECOREMATCHER_H
