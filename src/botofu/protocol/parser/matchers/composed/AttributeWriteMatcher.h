#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTEWRITEMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTEWRITEMATCHER_H

#include <memory>

#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"
#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/WriteAttributeMethodCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  output.[writemethod](this.[attribute]);
 *  ================================
 */
struct AttributeWriteMatcher final : public BaseComposedMatcher {

    AttributeWriteMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                          DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

private:

    std::shared_ptr<WriteAttributeMethodCoreMatcher> m_write_attribute_method_matcher;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTEWRITEMATCHER_H
