#include "AttributeWriteMatcher.h"

#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE) MEMBER (std::make_shared< TYPE >(constant_pool_info))

AttributeWriteMatcher::AttributeWriteMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                             DofusMethod const &method)
        : BaseComposedMatcher(constant_pool_info, method),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_write_attribute_method_matcher,
                                           WriteAttributeMethodCoreMatcher) {
    this->add_matcher(m_write_attribute_method_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void AttributeWriteMatcher::update_fields(FieldsType &fields) const {
    SPDLOG_DEBUG("Matched '{}' at instruction n°{} of method '{}'.",
                 m_write_attribute_method_matcher->method.to_string(),
                 m_write_attribute_method_matcher->get_match_instruction_index(),
                 p_method.to_string());
    DofusAttribute const &attribute = m_write_attribute_method_matcher->attribute;
    if (!this->check_field(fields, attribute)) {
        return;
    }
    ClassField &field = fields[attribute.get_attribute_name()];
    field.write_method = m_write_attribute_method_matcher->method;
}
