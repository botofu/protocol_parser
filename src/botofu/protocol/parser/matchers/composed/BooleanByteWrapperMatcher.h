#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERMATCHER_H

#include <memory>

#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"
#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/BooleanByteWrapperSetFlagCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  [unknown_name] = BooleanByteWrapper.setFlag([unknown_name], [bit_index], this.[attribute]);
 *  ================================
 */
struct BooleanByteWrapperMatcher final : public BaseComposedMatcher {

    BooleanByteWrapperMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                              DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

    void update_matchers(std::vector<Instr> const &instructions, std::size_t match_position) final;

private:

    std::size_t                                           m_number_of_bit_fields_found_yet;
    std::shared_ptr<BooleanByteWrapperSetFlagCoreMatcher> m_boolean_byte_wrapper_matcher;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERMATCHER_H
