#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_RUNTIMELENLISTSERIALIZEASMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_RUNTIMELENLISTSERIALIZEASMATCHER_H

#include <memory>

#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/WriteAttributeAttributeMethodCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/ForLoopStartCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/ForLoopEndAttributeAttributeCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/SerializeCallWithTypeManagerCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/DebugLineInstructionCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  output.[writemethod1]](this.[attribute].length);
 *  for(var [i]:uint = 0; [i]] < this.[attribute].length; [i]++)
 *  {
 *      (this.[attribute][[i] as [...]).serializeAs_[...](output);
 *  }
 *  ================================
 */
struct RuntimeLenListSerializeAsMatcher final : public BaseComposedMatcher {

    RuntimeLenListSerializeAsMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                     DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

private:

    std::shared_ptr<DebugLineInstructionCoreMatcher> m_debugline_matcher;

    std::shared_ptr<WriteAttributeAttributeMethodCoreMatcher> m_write_attribute_attribute_method_matcher;
    std::shared_ptr<ForLoopStartCoreMatcher>                  m_for_loop_start_matcher;
    std::shared_ptr<SerializeCallWithTypeManagerCoreMatcher>  m_serialize_call_with_type_manager_matcher;
    std::shared_ptr<ForLoopEndAttributeAttributeCoreMatcher>  m_for_loop_end_attribute_attribute_matcher;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_RUNTIMELENLISTSERIALIZEASMATCHER_H
