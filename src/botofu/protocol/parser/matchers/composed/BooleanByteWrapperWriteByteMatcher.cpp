#include "BooleanByteWrapperWriteByteMatcher.h"

#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE) MEMBER (std::make_shared< TYPE >(constant_pool_info))

BooleanByteWrapperWriteByteMatcher::BooleanByteWrapperWriteByteMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                                                       DofusMethod const &method)
        : BaseComposedMatcher(constant_pool_info, method),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_boolean_byte_wrapper_write_byte_matcher,
                                           BooleanByteWrapperWriteByteCoreMatcher) {
    this->add_matcher(m_boolean_byte_wrapper_write_byte_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void
BooleanByteWrapperWriteByteMatcher::update_fields([[maybe_unused]] FieldsType &fields) const {
    SPDLOG_DEBUG("Matched BooleanByteWrapper byte write at instruction n°{} in function '{}'.",
                 m_boolean_byte_wrapper_write_byte_matcher->get_match_instruction_index(),
                 p_method.to_string());
    // Do nothing here, it is not a write we should record.
}
