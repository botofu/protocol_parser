#include "UnmatchedWriteMatcher.h"

#include <spdlog/spdlog.h>

UnmatchedWriteMatcher::UnmatchedWriteMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                             DofusMethod const &method)
        : BaseComposedMatcher(constant_pool_info, method),
          m_prop_void_matcher(std::make_shared<SingleCallPropVoidCoreMatcher>(p_constant_pool_info)) {
    this->add_matcher(m_prop_void_matcher);
}

void UnmatchedWriteMatcher::update_fields([[maybe_unused]] FieldsType &fields) const {
    if (m_prop_void_matcher->is_valid) {
        SPDLOG_WARN("Unmatched '{}' in method '{}' at instruction n°{}.",
                    m_prop_void_matcher->called_method.to_string(),
                    p_method.to_string(),
                    m_prop_void_matcher->get_match_instruction_index());
    }
}
