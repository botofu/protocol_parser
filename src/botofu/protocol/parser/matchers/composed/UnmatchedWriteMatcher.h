#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_UNMATCHEDWRITEMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_UNMATCHEDWRITEMATCHER_H

#include <memory>

#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"
#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/SingleCallPropVoidCoreMatcher.h"

/**
 * Match the unmatched calls to
 *
 *  ================================
 *  output.[writemethod/serialize/serializeAs](this.[attribute])
 *  ================================
 */
struct UnmatchedWriteMatcher final : public BaseComposedMatcher {

    UnmatchedWriteMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                          DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

private:

    std::shared_ptr<SingleCallPropVoidCoreMatcher> m_prop_void_matcher;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_UNMATCHEDWRITEMATCHER_H
