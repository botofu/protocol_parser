#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_SUPERSERIALIZEASMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_SUPERSERIALIZEASMATCHER_H

#include <memory>

#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"
#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/SuperSerializeAsCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  super.serializeAs_[...](output);
 *  ================================
 */
struct SuperSerializeAsMatcher final : public BaseComposedMatcher {

    SuperSerializeAsMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                            DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

private:

    std::shared_ptr<SuperSerializeAsCoreMatcher> m_serialize_as_attribute_matcher;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_SUPERSERIALIZEASMATCHER_H
