#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTEWRITELOWERBOUNDCHECK_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTEWRITELOWERBOUNDCHECK_H

#include <memory>

#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/ComparisonLessThanCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/ThrowExceptionCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/WriteAttributeMethodCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/DebugLineInstructionCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  if([attribute] < [val1]) {
 *      throw new Error("Forbidden value (" + this.[attribute] + ") on element sourceId.");
 *  }
 *  output.[writemethod](this.[attribute]);
 *  ================================
 */
struct AttributeWriteLowerBoundCheck final : public BaseComposedMatcher {

    explicit AttributeWriteLowerBoundCheck(abc::ConstantPoolInfo const &constant_pool_info,
                                           DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

private:

    std::shared_ptr<DebugLineInstructionCoreMatcher> m_debugline_matcher;

    std::shared_ptr<ComparisonLessThanCoreMatcher>   m_comparison_less_than_matcher;
    std::shared_ptr<ThrowExceptionCoreMatcher>       m_throw_matcher;
    std::shared_ptr<WriteAttributeMethodCoreMatcher> m_write_attribute_matcher;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTEWRITELOWERBOUNDCHECK_H
