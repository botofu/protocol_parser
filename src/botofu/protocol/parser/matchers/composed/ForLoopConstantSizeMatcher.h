#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_FORLOOPCONSTANTSIZEMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_FORLOOPCONSTANTSIZEMATCHER_H

#include <memory>

#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/ForLoopStartCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/DebugLineInstructionCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/ArrayAttributeIndexingWriteCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/ForLoopEndConstantSizeCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  for(var [i]:uint = 0; [i]] < [constant]; [i]++)
 *  {
 *      output.[writemethod](this.[attribute][[i]]);
 *  }
 *  ================================
 */
struct ForLoopConstantSizeMatcher final : public BaseComposedMatcher {

    ForLoopConstantSizeMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                               DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

private:

    std::shared_ptr<DebugLineInstructionCoreMatcher> m_debugline_matcher;

    std::shared_ptr<ForLoopStartCoreMatcher>                m_for_loop_start_matcher;
    std::shared_ptr<ArrayAttributeIndexingWriteCoreMatcher> m_array_indexing_matcher;
    std::shared_ptr<ForLoopEndConstantSizeCoreMatcher>      m_for_loop_end_matcher;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_FORLOOPCONSTANTSIZEMATCHER_H
