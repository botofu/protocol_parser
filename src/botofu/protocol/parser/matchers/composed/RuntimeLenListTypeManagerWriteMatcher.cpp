#include "RuntimeLenListTypeManagerWriteMatcher.h"

#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE) MEMBER (std::make_shared< TYPE >(constant_pool_info))

RuntimeLenListTypeManagerWriteMatcher::RuntimeLenListTypeManagerWriteMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                                                             DofusMethod const &method)
        : BaseComposedMatcher(constant_pool_info, method),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_debugline_matcher, DebugLineInstructionCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_write_attribute_attribute_method_matcher,
                                           WriteAttributeAttributeMethodCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_for_loop_start_matcher, ForLoopStartCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_write_type_with_type_manager_matcher,
                                           WriteTypeWithTypeManagerCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_serialize_call_with_type_manager_matcher,
                                           SerializeCallWithTypeManagerCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_for_loop_end_attribute_attribute_matcher,
                                           ForLoopEndAttributeAttributeCoreMatcher) {
    this->add_matcher(m_write_attribute_attribute_method_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_for_loop_start_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_write_type_with_type_manager_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_serialize_call_with_type_manager_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_for_loop_end_attribute_attribute_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void
RuntimeLenListTypeManagerWriteMatcher::update_fields(FieldsType &fields) const {
    SPDLOG_DEBUG("Matched TypeManager list serialisation with type ID starting at "
                 "instruction n°{} of method '{}'.",
                 m_write_attribute_attribute_method_matcher->get_match_instruction_index(),
                 p_method.to_string());
    DofusAttribute const &attribute = m_write_attribute_attribute_method_matcher->attribute;
    if (!this->check_field(fields, attribute)) {
        return;
    }
    ClassField &field = fields[attribute.get_attribute_name()];
    field.self_serialize_method = m_serialize_call_with_type_manager_matcher->method;
    field.write_length_method   = m_write_attribute_attribute_method_matcher->method;
    field.prefixed_by_type_id   = true;
    field.write_type_id_method  = m_write_type_with_type_manager_matcher->write_method;
}
