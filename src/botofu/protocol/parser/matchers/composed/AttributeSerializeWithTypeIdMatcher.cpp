#include "AttributeSerializeWithTypeIdMatcher.h"

#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE) MEMBER (std::make_shared< TYPE >(constant_pool_info))

AttributeSerializeWithTypeIdMatcher::AttributeSerializeWithTypeIdMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                                                         DofusMethod const &method)
        : BaseComposedMatcher(constant_pool_info, method),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_debugline_matcher, DebugLineInstructionCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_write_type_id_matcher,
                                           WriteAttributeTypeIdCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_attribute_serialize_matcher,
                                           AttributeSerializeCoreMatcher) {
    this->add_matcher(m_write_type_id_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_attribute_serialize_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void
AttributeSerializeWithTypeIdMatcher::update_fields(FieldsType &fields) const {
    SPDLOG_DEBUG("Matched serialization of attribute '{}' prefixed by its type ID at "
                 "instruction n°{} of method '{}'.",
                 m_attribute_serialize_matcher->attribute.to_string(),
                 m_attribute_serialize_matcher->get_match_instruction_index(),
                 p_method.to_string());
    DofusAttribute const &attribute = m_attribute_serialize_matcher->attribute;
    if (!this->check_field(fields, attribute)) {
        return;
    }
    ClassField &field = fields[attribute.get_attribute_name()];
    field.prefixed_by_type_id   = true;
    field.self_serialize_method = m_attribute_serialize_matcher->method;
    field.write_type_id_method  = m_write_type_id_matcher->method;
}
