#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTESERIALIZEWITHTYPEIDMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTESERIALIZEWITHTYPEIDMATCHER_H

#include <memory>

#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"
#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/AttributeSerializeCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/DebugLineInstructionCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/WriteAttributeTypeIdCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  output.[writemethod](this.[attribute].getTypeId());
 *  this.[attribute].[serialize/serializeAs_...](output);
 *  ================================
 */
struct AttributeSerializeWithTypeIdMatcher final : public BaseComposedMatcher {

    AttributeSerializeWithTypeIdMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                        DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

private:

    std::shared_ptr<DebugLineInstructionCoreMatcher> m_debugline_matcher;

    std::shared_ptr<WriteAttributeTypeIdCoreMatcher> m_write_type_id_matcher;
    std::shared_ptr<AttributeSerializeCoreMatcher>   m_attribute_serialize_matcher;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTESERIALIZEWITHTYPEIDMATCHER_H
