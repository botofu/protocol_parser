#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_RUNTIMELENLISTNUMERICTYPE_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_RUNTIMELENLISTNUMERICTYPE_H

#include <memory>

#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/WriteAttributeAttributeMethodCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/ForLoopStartCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/ForLoopEndAttributeAttributeCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/SerializeCallWithTypeManagerCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/ComparisonLessThanArrayIndexingCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/ThrowExceptionArrayIndexingCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/ArrayAttributeIndexingWriteCoreMatcher.h"
#include "botofu/protocol/parser/matchers/core/DebugLineInstructionCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  output.[writemethod1]](this.[attribute].length);
 *  for(var [i]:uint = 0; [i]] < this.[attribute].length; [i]++)
 *  {
 *      output.[writemethod2](this.[attribute][[i]]);
 *  }
 *  ================================
 */
struct RuntimeLenListNumericType final : public BaseComposedMatcher {

    RuntimeLenListNumericType(abc::ConstantPoolInfo const &constant_pool_info,
                              DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

private:

    std::shared_ptr<DebugLineInstructionCoreMatcher> m_debugline_matcher;

    std::shared_ptr<WriteAttributeAttributeMethodCoreMatcher> m_write_attribute_attribute_matcher;
    std::shared_ptr<ForLoopStartCoreMatcher>                  m_for_loop_start_matcher;
    std::shared_ptr<ArrayAttributeIndexingWriteCoreMatcher>   m_array_indexing_matcher;
    std::shared_ptr<ForLoopEndAttributeAttributeCoreMatcher>  m_for_loop_end_matcher;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_RUNTIMELENLISTNUMERICTYPE_H
