#include "BaseComposedMatcher.h"

#include <utility>
#include <spdlog/spdlog.h>

BaseComposedMatcher::BaseComposedMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                         DofusMethod const &method)
        : p_constant_pool_info(constant_pool_info),
          p_core_matchers(),
          p_method(method),
          m_pattern_length{0} {
    for (auto const &matcher : p_core_matchers) {
        this->m_pattern_length += matcher->size();
    }
}

void BaseComposedMatcher::add_matcher(const std::shared_ptr<BaseCoreMatcher> &matcher) {
    this->m_pattern_length += matcher->size();
    this->p_core_matchers.push_back(matcher);
}

bool BaseComposedMatcher::match_pattern(std::vector<Instr> const &instructions,
                                        std::size_t start) const {
    for (auto const &matcher : p_core_matchers) {
        if (matcher->match_pattern(instructions, start)) {
            start += matcher->size();
        } else { // No match
            return false;
        }
    }
    return true;
}

std::size_t
BaseComposedMatcher::find_pattern(std::vector<Instr> const &instructions,
                                  std::size_t start) const {
    // If there are not enough instructions to match, return a "no match".
    if (instructions.size() - start <= m_pattern_length) { return instructions.size(); }

    std::size_t const max_start_search{instructions.size() - m_pattern_length + 1};
    for (std::size_t  i{start}; i < max_start_search; ++i) {
        if (this->match_pattern(instructions, i)) {
            return i;
        }
    }
    return instructions.size();
}

template <typename MatcherType, typename... Args>
void BaseComposedMatcher::add_matcher(Args... args) {
    this->add_matcher(std::make_unique<MatcherType>(std::forward<Args>(args)...));
}

std::size_t BaseComposedMatcher::size() const {
    return m_pattern_length;
}

void BaseComposedMatcher::update_matchers(std::vector<Instr> const &instructions,
                                          std::size_t match_position) {
    for (auto &matcher : p_core_matchers) {
        matcher->update(instructions, match_position);
        match_position += matcher->size();
    }
}

std::size_t
BaseComposedMatcher::find_pattern_and_update_matchers(std::vector<Instr> const &instructions,
                                                      std::size_t start) {
    std::size_t const pattern_found{this->find_pattern(instructions, start)};
    if (pattern_found < instructions.size()) {
        this->update_matchers(instructions, pattern_found);
    }
    return pattern_found;
}

bool BaseComposedMatcher::check_field(const BaseComposedMatcher::FieldsType &fields,
                                      DofusAttribute const &attribute) const {
    std::string const &field_name{attribute.get_attribute_name()};
    if (fields.find(field_name) == fields.end()) {
        SPDLOG_ERROR("Field '{}' not registered when calling update_fields.",
                     attribute.to_string());
        return false;
    }
    return true;
}
