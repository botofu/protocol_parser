#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_CHECKIFNULLSERIALIZEASMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_CHECKIFNULLSERIALIZEASMATCHER_H

#include <memory>

#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/CheckIfNullSerializeAsCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  if(this.[attribute] == null)
 *  {
 *      output.[writemethod1](0);
 *  }
 *  else
 *  {
 *      output.[writemethod1](1);
 *      this.[attribute].[serialize/serializeAs](output);
 *  }
 *  ================================
 */
struct CheckIfNullSerializeAsMatcher final : public BaseComposedMatcher {

    CheckIfNullSerializeAsMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                  DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

private:

    std::shared_ptr<CheckIfNullSerializeAsCoreMatcher> m_check_if_null_serialize_as_matcher;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_CHECKIFNULLSERIALIZEASMATCHER_H
