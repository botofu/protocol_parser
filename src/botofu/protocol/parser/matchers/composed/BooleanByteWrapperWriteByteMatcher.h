#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERWRITEBYTEMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERWRITEBYTEMATCHER_H

#include <memory>

#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/BooleanByteWrapperWriteByteCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  output.writeByte([unknown_name]);
 *  ================================
 */
struct BooleanByteWrapperWriteByteMatcher final : public BaseComposedMatcher {

    BooleanByteWrapperWriteByteMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                       DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

private:

    std::shared_ptr<BooleanByteWrapperWriteByteCoreMatcher> m_boolean_byte_wrapper_write_byte_matcher;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_BOOLEANBYTEWRAPPERWRITEBYTEMATCHER_H
