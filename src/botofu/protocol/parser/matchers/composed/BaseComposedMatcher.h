#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_BASECOMPOSEDMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_BASECOMPOSEDMATCHER_H

#include <memory>
#include <vector>
#include <regex>
#include <unordered_map>

#include "botofu/protocol/parser/matchers/core/BaseCoreMatcher.h"
#include "botofu/protocol/parser/data_structures/ClassField.h"
#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"

struct BaseComposedMatcher {

    using FieldsType = std::unordered_map<std::string /*field name*/, ClassField>;

    BaseComposedMatcher(abc::ConstantPoolInfo const &constant_pool_info, DofusMethod const &method);

    /**
     * @brief Adds the given core matcher to the ComposedMatcher.
     * @param matcher the core matcher to add.
     */
    void add_matcher(const std::shared_ptr<BaseCoreMatcher> &matcher);

    /**
     * @brief Constructs an instance of the given @p MatcherType with the provided @p args
     *        and adds it to the ComposedMatcher instance.
     * @tparam MatcherType type of the matcher to construct and add
     * @tparam Args types of the arguments forwarded to the matcher constructor
     * @param args arguments forwarded to the matched constructor
     */
    template <typename MatcherType, typename... Args>
    void add_matcher(Args... args);

    /**
     * @brief Find the pattern stored by the instance in the given sequence of instructions.
     * @param instructions the list of instructions we want to search in.
     * @param start index of the first considered instruction in @p instruction.
     * @return the index of the first instruction matching the pattern or instruction.size() if the
     *         pattern could not be found in the instruction list.
     */
    [[nodiscard]] std::size_t
    find_pattern(std::vector<Instr> const &instructions, std::size_t start = 0) const;

    /**
     * @brief Find the pattern stored by the instance in the given sequence of instructions and
     *        update the corresponding matchers
     * @param instructions the list of instructions we want to search in.
     * @param start index of the first considered instruction in @p instruction.
     * @return the index of the first instruction matching the pattern or instruction.size() if the
     *         pattern could not be found in the instruction list.
     */
    [[nodiscard]] std::size_t
    find_pattern_and_update_matchers(std::vector<Instr> const &instructions, std::size_t start = 0);

    /**
     * @brief Check if the instructions starting at @p start match the stored pattern.
     * @param instructions the list of instructions we want to search in.
     * @param start index of the first considered instruction in @p instruction.
     * @return true if instructions[start] and the following instructions match the provided
     *         pattern, else false.
     */
    [[nodiscard]] bool
    match_pattern(std::vector<Instr> const &instructions, std::size_t start) const;

    /**
     * @brief Update the @p fields structure according to the matched pattern
     * @param fields the structure containing the fields information
     */
    virtual void update_fields(FieldsType &fields) const = 0;

    /**
     * @brief Update the internal CoreMatchers
     * @param instructions instructions
     * @param match_position index of the first instruction of a match
     * @pre this->match_pattern(instructions, match_position) == true
     */
    virtual void
    update_matchers(std::vector<Instr> const &instructions, std::size_t match_position);

    /**
     * @brief Return the size of the pattern matched by the instance
     * @return the size of the pattern matched by the instance
     */
    [[nodiscard]] std::size_t size() const;

protected:

    [[nodiscard]] bool check_field(FieldsType const &fields, DofusAttribute const &attribute) const;

    abc::ConstantPoolInfo const                   &p_constant_pool_info;
    std::vector<std::shared_ptr<BaseCoreMatcher>> p_core_matchers;
    DofusMethod const                             &p_method;

private:

    std::size_t m_pattern_length;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_BASECOMPOSEDMATCHER_H
