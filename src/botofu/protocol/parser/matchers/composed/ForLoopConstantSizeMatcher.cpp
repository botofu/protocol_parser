#include "ForLoopConstantSizeMatcher.h"

#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE) MEMBER (std::make_shared< TYPE >(constant_pool_info))

ForLoopConstantSizeMatcher::ForLoopConstantSizeMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                                       DofusMethod const &method)
        : BaseComposedMatcher(constant_pool_info, method),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_debugline_matcher, DebugLineInstructionCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_for_loop_start_matcher, ForLoopStartCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_array_indexing_matcher,
                                           ArrayAttributeIndexingWriteCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_for_loop_end_matcher,
                                           ForLoopEndConstantSizeCoreMatcher) {
    this->add_matcher(m_for_loop_start_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_array_indexing_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_for_loop_end_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void ForLoopConstantSizeMatcher::update_fields(FieldsType &fields) const {
    SPDLOG_DEBUG("Matched a constant for loop for attribute '{}' at instruction n°{} "
                 "in method '{}'.",
                 m_array_indexing_matcher->attribute.to_string(),
                 m_for_loop_start_matcher->get_match_instruction_index(),
                 p_method.to_string());
    DofusAttribute const &attribute = m_array_indexing_matcher->attribute;
    if (!this->check_field(fields, attribute)) {
        return;
    }
    ClassField &field = fields[attribute.get_attribute_name()];
    field.constant_length = m_for_loop_end_matcher->loop_size;
    field.write_method    = m_array_indexing_matcher->write_method;
}
