#include "RuntimeLenListBoundCheckMatcher.h"

#include <spdlog/spdlog.h>

#define BOTOFU_INITIALISE_MATCHER_MEMBER(MEMBER, TYPE) MEMBER (std::make_shared< TYPE >(constant_pool_info))

RuntimeLenListBoundCheckMatcher::RuntimeLenListBoundCheckMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                                                 DofusMethod const &method)
        : BaseComposedMatcher(constant_pool_info, method),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_debugline_matcher, DebugLineInstructionCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_write_attribute_attribute_matcher,
                                           WriteAttributeAttributeMethodCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_for_loop_start_matcher, ForLoopStartCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_comparison_less_than_matcher,
                                           ComparisonLessThanArrayIndexingCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_throw_exception_matcher,
                                           ThrowExceptionArrayIndexingCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_array_indexing_matcher,
                                           ArrayAttributeIndexingWriteCoreMatcher),
          BOTOFU_INITIALISE_MATCHER_MEMBER(m_for_loop_end_matcher,
                                           ForLoopEndAttributeAttributeCoreMatcher) {
    this->add_matcher(m_write_attribute_attribute_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_for_loop_start_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_comparison_less_than_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_throw_exception_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_array_indexing_matcher);
    this->add_matcher(m_debugline_matcher);
    this->add_matcher(m_for_loop_end_matcher);
}

#undef BOTOFU_INITIALISE_MATCHER_MEMBER

void RuntimeLenListBoundCheckMatcher::update_fields(FieldsType &fields) const {
    SPDLOG_DEBUG("Matched numeric type list serialisation with one bound checked at "
                 "instruction n°{} in function '{}'.",
                 m_write_attribute_attribute_matcher->get_match_instruction_index(),
                 p_method.to_string());
    DofusAttribute const &attribute = m_array_indexing_matcher->attribute;
    if (!this->check_field(fields, attribute)) {
        return;
    }
    ClassField &field = fields[attribute.get_attribute_name()];
    field.write_method        = m_array_indexing_matcher->write_method;
    field.write_length_method = m_write_attribute_attribute_matcher->method;
    field.bounds.lower        = m_comparison_less_than_matcher->limits.lower;
}
