#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTECALLSERIALIZEMETHODMATCHER_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTECALLSERIALIZEMETHODMATCHER_H

#include <memory>

#include "botofu/protocol/parser/dofus_interface/DofusMethod.h"
#include "botofu/protocol/parser/matchers/composed/BaseComposedMatcher.h"
#include "botofu/protocol/parser/matchers/core/AttributeSerializeCoreMatcher.h"

/**
 * Match the following kind of AS3 code:
 *
 *  ================================
 *  this.[attribute].[serialize/serializeAs_...](output);
 *  ================================
 */
struct AttributeCallSerializeMethodMatcher final : public BaseComposedMatcher {

    AttributeCallSerializeMethodMatcher(abc::ConstantPoolInfo const &constant_pool_info,
                                        DofusMethod const &method);

    void update_fields(FieldsType &fields) const final;

private:

    std::shared_ptr<AttributeSerializeCoreMatcher> m_attribute_serialize_matcher;

};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_COMPOSED_ATTRIBUTECALLSERIALIZEMETHODMATCHER_H
