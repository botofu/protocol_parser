#include "io.h"

#include <boost/algorithm/string/predicate.hpp>

using boost::algorithm::contains;
using boost::algorithm::ends_with;

IOMethodType get_io_method_type_from_name(std::string const &name) {
    if (ends_with(name, "Boolean")) { return IOMethodType::BOOL; }
    else if (ends_with(name, "Byte")) { return IOMethodType::INT8; }
    else if (ends_with(name, "UnsignedByte")) {
        return IOMethodType::UINT8;
    } else if (ends_with(name, "Short")) { return IOMethodType::INT16; }
    else if (ends_with(name, "UnsignedShort") || ends_with(name, "UhShort")) {
        return IOMethodType::UINT16;
    } else if (ends_with(name, "Int")) { return IOMethodType::INT32; }
    else if (ends_with(name, "UnsignedInt") || ends_with(name, "UhInt")) {
        return IOMethodType::UINT32;
    } else if (ends_with(name, "Long")) { return IOMethodType::INT64; }
    else if (ends_with(name, "UnsignedLong") || ends_with(name, "UhLong")) {
        return IOMethodType::UINT64;
    } else if (ends_with(name, "Float")) { return IOMethodType::FLOAT; }
    else if (ends_with(name, "Double")) { return IOMethodType::DOUBLE; }
    else if (ends_with(name, "UTF")) { return IOMethodType::UTF; }
    else if (ends_with(name, "Bytes")) { return IOMethodType::BYTES; }
    return IOMethodType::UNKNOWN;
}

bool is_var_io_method_from_name(std::string const &name) {
    return contains(name, "Var");
}
