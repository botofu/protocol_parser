#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_INSTRUCTIONS_PUSH_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_INSTRUCTIONS_PUSH_H

#include <vector>
#include <string>

#include "botofu/swf/parser/abc/MethodBody/AVM2Instructions.h"
#include "botofu/swf/parser/abc/ConstantPoolInfo.h"

enum struct PushInstructionType {
    BYTE, DOUBLE, INT, UINT, SHORT, UNKNOWN
};

PushInstructionType get_push_type_from_name(std::string const &name);

std::string get_push_representation_from_type(PushInstructionType type,
                                              abc::ConstantPoolInfo const &constant_pool_info,
                                              Instr const &instruction);

std::string const push_instruction_regex{"push((byte)|(double)|(int)|(uint)|(short))"};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_MATCHERS_INSTRUCTIONS_PUSH_H
