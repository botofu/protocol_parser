#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSMETHOD_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSMETHOD_H

#include <string>

#include "DofusNamespace.h"
#include "botofu/protocol/parser/matchers/instructions/io.h"

class DofusMethod {

public:
    DofusMethod() = default;

    explicit DofusMethod(std::string const &method);

    [[nodiscard]] DofusNamespace get_namespace() const;

    [[nodiscard]] std::string get_method_name() const;

    [[nodiscard]] std::string get_class_name() const;

    [[nodiscard]] std::string to_string() const;

    [[nodiscard]] IOMethodType get_method_type() const;

    [[nodiscard]] bool is_var_method() const;

private:
    std::string    m_method_name;
    std::string    m_class_name;
    DofusNamespace m_namespace;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_DOFUSMETHOD_H
