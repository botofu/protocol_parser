#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DOFUS_INTERFACE_DOFUSNAMESPACE_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DOFUS_INTERFACE_DOFUSNAMESPACE_H

#include <string>
#include <vector>

#include <boost/functional/hash.hpp>

class DofusNamespace {

public:

    DofusNamespace() = default;

    explicit DofusNamespace(std::string const &dofus_namespace);

    bool operator==(DofusNamespace const &other) const;

    [[nodiscard]] std::vector<std::string> const &get_namespace_parts() const;

    [[nodiscard]] std::string to_string() const;

private:

    std::vector<std::string> m_namespace_parts;

};

namespace std {

    template <>
    struct hash<DofusNamespace> {
        std::size_t operator()(const DofusNamespace &dofus_namespace) const;
    };

}

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DOFUS_INTERFACE_DOFUSNAMESPACE_H
