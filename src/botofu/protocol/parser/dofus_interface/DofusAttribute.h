#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DOFUS_INTERFACE_DOFUSATTRIBUTE_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DOFUS_INTERFACE_DOFUSATTRIBUTE_H

#include <string>
#include <boost/functional/hash.hpp>

#include "DofusNamespace.h"

class DofusAttribute {

public:
    DofusAttribute() = default;

    explicit DofusAttribute(std::string const &name);

    [[nodiscard]] bool is_private() const;

    [[nodiscard]] DofusNamespace const &get_namespace() const;

    [[nodiscard]] std::string const &get_class_name() const;

    [[nodiscard]] std::string const &get_attribute_name() const;

    [[nodiscard]] std::string to_string() const;

    bool operator==(DofusAttribute const &other) const;

    void set_attribute_name(std::string const &name);

private:

    DofusNamespace m_namespace;
    std::string    m_class_name;
    std::string    m_attribute_name;

};

namespace std {

    template <>
    struct hash<DofusAttribute> {
        std::size_t operator()(const DofusAttribute &dofus_attribute) const;
    };

}

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DOFUS_INTERFACE_DOFUSATTRIBUTE_H
