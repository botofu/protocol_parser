#include "botofu/swf/parser/abc/constants.h"
#include "botofu/protocol/parser/dofus_interface/DofusAttribute.h"

DofusAttribute::DofusAttribute(std::string const &name) {
    std::size_t const namespace_class_sep{name.find(abc::NAMESPACE_CLASS_SEPARATOR)};
    std::size_t const class_attribute_sep{name.find(abc::CLASS_ATTRIBUTE_SEPARATOR,
                                                    namespace_class_sep + 1)};
    if (namespace_class_sep == std::string::npos && class_attribute_sep == std::string::npos) {
        // m_namespace already default_constructed
        // m_class_name already default constructed
        m_attribute_name = name;
    } else {
        m_namespace      = DofusNamespace(name.substr(0, namespace_class_sep));
        m_class_name     = name.substr(namespace_class_sep + 1,
                                       class_attribute_sep - namespace_class_sep - 1);
        m_attribute_name = name.substr(class_attribute_sep + 1);
    }
}

bool DofusAttribute::is_private() const {
    return !m_attribute_name.empty() && m_attribute_name[0] == '_';
}

DofusNamespace const &DofusAttribute::get_namespace() const {
    return m_namespace;
}

std::string const &DofusAttribute::get_class_name() const {
    return m_class_name;
}

std::string const &DofusAttribute::get_attribute_name() const {
    return m_attribute_name;
}

bool DofusAttribute::operator==(DofusAttribute const &other) const {
    return m_attribute_name == other.m_attribute_name && m_class_name == other.m_class_name &&
           m_namespace == other.m_namespace;
}

std::string DofusAttribute::to_string() const {
    std::string result{m_namespace.to_string()};
    if (!result.empty()) { result += "."; }
    result += m_class_name;
    if (!m_class_name.empty()) { result += "."; }
    result += m_attribute_name;
    return result;
}

void DofusAttribute::set_attribute_name(std::string const &name) {
    m_attribute_name = name;
}

namespace std {

    std::size_t hash<DofusAttribute>::operator()(const DofusAttribute &dofus_attribute) const {
        std::size_t seed{0};

        boost::hash_combine(seed, dofus_attribute.get_attribute_name());
        boost::hash_combine(seed, dofus_attribute.get_class_name());
        boost::hash_combine(seed, std::hash<DofusNamespace>()(dofus_attribute.get_namespace()));
        return seed;
    }
}