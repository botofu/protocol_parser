#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DOFUS_INTERFACE_DOFUSTYPE_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DOFUS_INTERFACE_DOFUSTYPE_H

#include <string>

#include "DofusNamespace.h"

class DofusType {

public:
    DofusType() = default;

    explicit DofusType(std::string const &type);

    [[nodiscard]] DofusNamespace get_namespace() const;

    [[nodiscard]] std::string get_type() const;

    [[nodiscard]] std::string to_string() const;

    [[nodiscard]] bool is_vector() const;

private:
    std::string    m_type;
    DofusNamespace m_namespace;
};

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DATA_STRUCTURES_DOFUS_INTERFACE_DOFUSTYPE_H
