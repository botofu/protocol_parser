#ifndef PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_LIMITS_H
#define PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_LIMITS_H

#include <string>

#include <boost/optional.hpp>
#include <nlohmann/json.hpp>

#include "DofusType.h"

using json = nlohmann::json;

struct Limits {

    operator bool() const;

    boost::optional<std::string> lower;
    boost::optional<std::string> upper;

};

void to_json(json &j, const Limits &limits);

#endif //PROTOCOL_PARSER_SRC_BOTOFU_PROTOCOL_PARSER_DOFUS_INTERFACE_LIMITS_H
