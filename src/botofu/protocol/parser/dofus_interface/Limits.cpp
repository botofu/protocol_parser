#include "Limits.h"

Limits::operator bool() const {
    return lower || upper;
}

void to_json(json &j, const Limits &limits) {
    if (limits.lower) { j["low"] = *limits.lower; }
    if (limits.upper) { j["up"] = *limits.upper; }
}
