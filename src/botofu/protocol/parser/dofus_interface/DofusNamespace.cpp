#include "DofusNamespace.h"

#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/join.hpp>

DofusNamespace::DofusNamespace(std::string const &dofus_namespace) {
    boost::split(m_namespace_parts, dofus_namespace, [](char const c) { return c == '.'; });
}

bool DofusNamespace::operator==(DofusNamespace const &other) const {
    std::size_t const size{m_namespace_parts.size()};
    if (size != other.m_namespace_parts.size()) { return false; }
    for (std::size_t i{0}; i < size; ++i) {
        if (m_namespace_parts[i] != other.m_namespace_parts[i]) { return false; }
    }
    return true;
}

std::vector<std::string> const &DofusNamespace::get_namespace_parts() const {
    return m_namespace_parts;
}

std::string DofusNamespace::to_string() const {
    return boost::join(m_namespace_parts, ".");
}

namespace std {
    std::size_t hash<DofusNamespace>::operator()(const DofusNamespace &dofus_namespace) const {
        std::size_t            seed{0};
        for (std::string const &namespace_part : dofus_namespace.get_namespace_parts()) {
            boost::hash_combine(seed, namespace_part);
        }
        return seed;
    }
}