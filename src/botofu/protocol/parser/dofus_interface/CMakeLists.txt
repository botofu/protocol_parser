target_sources(protocol_parser PUBLIC
        DofusNamespace.cpp
        DofusType.cpp
        DofusAttribute.cpp
        DofusMethod.cpp
        Limits.cpp
        )
target_sources(protocol_parser INTERFACE
        DofusNamespace.h
        DofusType.h
        DofusAttribute.h
        DofusMethod.h
        Limits.h
        )
