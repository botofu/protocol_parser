# `botofu.protocol_parser`

Parse the `DofusInvoker.swf`, outputs a JSON. Simple.

## How to install

### Using `docker` (recommended)

```shell script
docker pull registry.gitlab.com/botofu/protocol_parser
# Assuming that you have the DofusInvoker.swf in /local/dir/ on your machine
docker run --rm -v /local/dir/:/data/ -it registry.gitlab.com/botofu/protocol_parser -i 2 /data/DofusInvoker.swf /data/protocol.json
# protocol.json is now in /local/dir/protocol.json 
```

### From source

Make sure that you have `git`, `cmake >= 3.14` and a C++17-compatible compiler 
installed.

#### Compiler support

I tested with `clang` 9 & 11 and it worked. 

#### Installation procedure

```shell script
git clone git@gitlab.com:botofu/protocol_parser.git
cd protocol_parser
mkdir build && cd build
# The next command will download dependencies and may take a long time
cmake -H. -DCMAKE_BUILD_TYPE=Release .. 
cmake --build ./ --target protocol_parser -- -j 4
# ./protocol_parser
```

## Usage

### Documentation 

```text
>>> ./protocol_parser --help
Protocol parser for the Botofu project
Usage: /usr/dev/protocol_parser [OPTIONS] DofusInvokerPath [ExportPath]

Positionals:
  DofusInvokerPath TEXT:FILE REQUIRED
                              Path to the DofusInvoker.swf
  ExportPath TEXT             Path to the file that will contain the output of the protocol parsing. If not provided, dump to the standard output.

Options:
  -h,--help                   Print this help message and exit
  -o,--out TEXT               Path to the file that will contain the output of the protocol parsing. If not provided, dump to the standard output.
  -i,--ident INT              Number of spaces used to indent resulting JSON. Default to minified (0).
  -a,--all,--no-default       No "default" entry in the resulting JSON. All the keys will be explicitly written, with a default value for the non-set keys.
```

## Output JSON format

The created JSON looks like
```text
{
  "default": {
    "field": {
      "boolean_byte_wrapper_position": -1,
      "bounds": null,
      "constant_length": 0,
      "default_value": "",
      "name": "",
      "namespace": "",
      "null_checked": false,
      "prefixed_by_type_id": false,
      "self_serialize_method": "",
      "type": "",
      "type_namespace": "",
      "use_boolean_byte_wrapper": false,
      "write_false_if_null_method": "",
      "write_length_method": "",
      "write_method": "",
      "write_type_id_method": ""
    }
  },
  "enumerations": [
    {
      "entries_type": "uint",
      "members": {
        "HOOK_POINT_CATEGORY_BASE_BACKGROUND": "4",
        "HOOK_POINT_CATEGORY_BASE_FOREGROUND": "6",
        "HOOK_POINT_CATEGORY_LIFTED_ENTITY": "3",
        "HOOK_POINT_CATEGORY_MERCHANT_BAG": "5",
        "HOOK_POINT_CATEGORY_MOUNT_DRIVER": "2",
        "HOOK_POINT_CATEGORY_PET": "1",
        "HOOK_POINT_CATEGORY_PET_FOLLOWER": "7",
        "HOOK_POINT_CATEGORY_UNDERWATER_BUBBLES": "8",
        "HOOK_POINT_CATEGORY_UNUSED": "0"
      },
      "name": "SubEntityBindingPointCategoryEnum"
    },
    // ...
  ],
  "messages": [
    {
      "fields": [
        {
          "default_value": "false",
          "name": "quiet",
          "type": "Boolean",
          "write_method": "writeBoolean"
        },
        {
          "default_value": "false",
          "name": "_isInitialized",
          "namespace": "com.ankamagames.dofus.network.messages.common.basic",
          "type": "Boolean"
        }
      ],
      "name": "BasicPingMessage",
      "namespace": "com.ankamagames.dofus.network.messages.common.basic",
      "protocolID": 182,
      "super": "NetworkMessage",
      "supernamespace": "com.ankamagames.jerakine.network",
      "use_hash_function": false
    },
    // ...
  ],
  "types": [
    {
      "fields": [
        {
          "bounds": {
            "low": "-9007199254740992.000000",
            "up": "9007199254740992.000000"
          },
          "default_value": "0",
          "name": "contextualId",
          "type": "Number",
          "write_method": "writeDouble"
        },
        {
          "name": "_dispositiontree",
          "namespace": "com.ankamagames.dofus.network.types.game.context",
          "type": "FuncTree",
          "type_namespace": "com.ankamagames.jerakine.network.utils"
        },
        {
          "name": "disposition",
          "prefixed_by_type_id": true,
          "self_serialize_method": "serialize",
          "type": "EntityDispositionInformations",
          "type_namespace": "com.ankamagames.dofus.network.types.game.context",
          "write_type_id_method": "writeShort"
        }
      ],
      "name": "GameContextActorPositionInformations",
      "namespace": "com.ankamagames.dofus.network.types.game.context",
      "protocolID": 566,
      "super": "",
      "supernamespace": "",
      "use_hash_function": false
    },
    // ...
  ],
  "version": {
    "client": {
      "build": {
        "date": {
          "day": 1,
          "full": "2020-4-1 13:45",
          "hour": 13,
          "minute": 45,
          "month": 4,
          "year": 2020
        }
      },
      "major": 2,
      "minor": 55,
      "patch": 1
    },
    "parser": {
      "major": 0,
      "minor": 2
    },
    "protocol": {
      "date": {
        "day": 13,
        "full": "2020-3-13 14:43",
        "hour": 14,
        "minute": 43,
        "month": 3,
        "year": 2020
      },
      "version": {
        "current": 1966,
        "minimum": 1966
      }
    }
  }
}
```

## TODO

1. Add documentation
2. Automated tests
