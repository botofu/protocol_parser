#include <fstream>
#include <CLI/CLI.hpp>
#include <spdlog/spdlog.h>

#include "botofu/swf/parser/SwfFile.h"
#include "botofu/swf/parser/tags/TagDoAbc.h"
#include "botofu/protocol/parser/data_structures/ClassInformation.h"
#include "botofu/protocol/parser/data_structures/EnumInformation.h"
#include "botofu/protocol/parser/data_structures/VersionFinder.h"

static void export_to_stream(std::ostream &out, json const &j, int indent) {
    if (indent == 0) {
        out << j.dump();
    } else {
        out << j.dump(indent);
    }
}

int main(int argc, char *argv[]) {

    // 1. Command line interface.
    CLI::App    app("Protocol parser for the Botofu project");
    std::string invoker_path, export_path;
    int         indent{0};
    bool        always_all_fields{false};
    app.add_option("DofusInvokerPath", invoker_path, "Path to the DofusInvoker.swf")
       ->required()
       ->check(CLI::ExistingFile);
    app.add_option("ExportPath,-o,--out",
                   export_path,
                   "Path to the file that will contain the output of the protocol parsing. "
                   "If not provided, dump to the standard output.");
    app.add_option("-i,--indent",
                   indent,
                   "Number of spaces used to indent resulting JSON. Default to minified (0).");
    app.add_flag("-a,--all,--no-default",
                 always_all_fields,
                 "No \"default\" entry in the resulting JSON. All the keys will be "
                 "explicitly written, with a default value for the non-set keys.");
    CLI11_PARSE(app, argc, argv);

    // 2. Load the SWF file provided and recover the DoABC tag.
    SPDLOG_INFO("Parsing SWF file provided: '{}'...", invoker_path);
    SwfFile file(invoker_path);
    auto    do_abc_tag_optional = file.get_first_tag_by_type(swf_constants::TagType::DOABC);
    if (!do_abc_tag_optional) {
        SPDLOG_ERROR("Could not find a DoABC tag in the SWF file provided.");
        return 1;
    }
    std::shared_ptr<TagDoAbc> const do_abc_tag = std::dynamic_pointer_cast<TagDoAbc>(*do_abc_tag_optional);

    // 3. Create the JSON that will gather the information
    json all_data;

    // 4. Extract all the versions available
    SPDLOG_INFO("Extracting versions...");
    VersionFinder version_finder(*do_abc_tag, invoker_path);
    all_data["version"] = version_finder.to_json();

    // 5. Write the default values if asked by the user
    ClassField            default_field;
    boost::optional<json> default_field_json;
    if (!always_all_fields) {
        default_field_json = default_field.to_json();
        all_data["default"]["field"] = *default_field_json;
    } else {
        default_field_json = boost::none;
        all_data["default"]["field"] = { };
    }

    // 6. Extract the network data
    SPDLOG_INFO("Extracting network data from {} classes definitions...",
                do_abc_tag->m_instances.size());
    abc::ConstantPoolInfo const &constant_pool_info{do_abc_tag->m_constant_pool_info};
    for (uint32_t               i{0}; i < do_abc_tag->m_instances.size(); ++i) {
        abc::InstanceInfo const &instance_info{do_abc_tag->m_instances[i]};
        // Do not consider the instances outside of the network namespace
        if (!instance_info.is_in_network_namespace(constant_pool_info)) { continue; }
        // Separate the enumerations & the messages/types
        if (instance_info.is_network_enumeration(constant_pool_info)) {
            EnumInformation enum_information(*do_abc_tag, i);
            all_data["enumerations"].push_back(enum_information.to_json());
        } else if (instance_info.is_network_message(constant_pool_info)) {
            ClassInformation message_information(*do_abc_tag, i);
            all_data["messages"].push_back(message_information.to_json(default_field_json));
        } else if (instance_info.is_network_type(constant_pool_info)) {
            ClassInformation type_information(*do_abc_tag, i);
            all_data["types"].push_back(type_information.to_json(default_field_json));
        }
    }

    // 7. Export the JSON to the given file.
    SPDLOG_INFO("Exporting {} enumerations, {} messages and {} types to '{}'.",
                all_data["enumerations"].size(),
                all_data["messages"].size(),
                all_data["types"].size(),
                export_path.empty() ? "stdout" : export_path);
    if (export_path.empty()) {
        export_to_stream(std::cout, all_data, indent);
    } else {
        std::ofstream out(export_path);
        export_to_stream(out, all_data, indent);
    }
}