include(ExternalProject)
include(FetchContent)

macro(get_external_dependency DEPENDENCY_NAME GIT_URL)

    message(STATUS "Downloading ${DEPENDENCY_NAME} from '${GIT_URL}' if not already available...")
    FetchContent_Declare(
            ${DEPENDENCY_NAME}
            GIT_REPOSITORY ${GIT_URL}
            GIT_PROGRESS TRUE
    )
    FetchContent_MakeAvailable(${DEPENDENCY_NAME})

endmacro()
