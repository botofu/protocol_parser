FROM alpine:3.11 AS builder
LABEL version="1.0" maintainer="Nelimee"

# See https://askubuntu.com/a/1013396
ENV DEBIAN_FRONTEND=noninteractive
# Install the dependencies
RUN apk update
RUN apk add cmake git build-base clang

RUN mkdir -p /usr/dev/build

WORKDIR /usr/dev
# Copy the needed files to the container
COPY ./src ./src
COPY CMakeLists.txt .
COPY main.cpp .
COPY ./cmake ./cmake

# Start the build
RUN cmake -DCMAKE_C_COMPILER=/usr/bin/clang -DCMAKE_CXX_COMPILER=/usr/bin/clang++ -DCMAKE_BUILD_TYPE=Release -Bbuild .
RUN cmake --build build/ --target protocol_parser -- -j 4

FROM alpine:3.11
LABEL version="1.0" maintainer="Nelimee"

RUN mkdir -p /usr/dev
WORKDIR /usr/dev

RUN apk update
RUN apk add libstdc++ libgcc

COPY --from=builder /usr/dev/build/protocol_parser .

ENTRYPOINT ["/usr/dev/protocol_parser"]